/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import businessLogic.Location;
import businessLogic.Sensor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import businessLogic.Vendor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author HP
 */
public class sensorDA {
     private Connection dbConnection;
     LocationDA location = new LocationDA();
     vendorDA vendor = new  vendorDA();
    public sensorDA(){
        dbConnection=DatabaseConnectionHandler.createConnection();
    }
    public static java.sql.Date convertFromJAVADateToSQLDate(java.util.Date javaDate) {
        java.sql.Date sqlDate = null;
        if (javaDate != null) {
            sqlDate = new java.sql.Date(javaDate.getTime());
        }
        return sqlDate;
    }
     public void insertSensor(Sensor sensor) throws SQLException{
        String insertSQL = "INSERT INTO sensor(sensor_type,sensor_status,date_installed,last_check_date,expected_expiry_date,location_id,vendor_id) VALUES(?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
        preparedStatement.setString(1, sensor.getSensorType());
        preparedStatement.setString(2, sensor.getSensorStatus());
        preparedStatement.setDate(3,convertFromJAVADateToSQLDate(new Date()) );
        preparedStatement.setDate(4,convertFromJAVADateToSQLDate(new Date()) );
        preparedStatement.setDate(5,convertFromJAVADateToSQLDate(sensor.getExpectedExpiryDate()));
        preparedStatement.setInt(6, sensor.getLocationId());
        preparedStatement.setInt(7, sensor.getVendorId());
        preparedStatement.execute( );
    
     }
  
     public List<Sensor> getsensorList(){
        ArrayList<Sensor> sensors=new ArrayList<Sensor>();
        try {
            String selectSQL = "SELECT * FROM sensor"; 
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery(selectSQL);
            while (rs.next()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int location_id=rs.getInt("location_id");
                int vendor_id=rs.getInt("vendor_id");
                Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                sensor.setLocationId(location_id);
                sensor.setVendorId(vendor_id);
                sensors.add(sensor);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensors;
    }
      public  int getLastSensorId() throws SQLException {
        
        String selectSQL = "SELECT sensor_id FROM sensor  ORDER BY sensor_id DESC LIMIT 1";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        ResultSet rs = preparedStatement.executeQuery(selectSQL );

        if (rs.next()) {
            return rs.getInt("sensor_id");
        }
        return -1;
    }
     public Sensor getSensorDetails(String location,String type) throws SQLException{
       
        String selectSQL = "SELECT * FROM sensor JOIN location  WHERE (sensor_type,location) VALUES (?,?)";
         PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setString(1, type);
            preparedStatement.setString(2, location);
            ResultSet rs = preparedStatement.executeQuery(selectSQL );
        if (rs.first()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                 Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                 return sensor;
        }
        return null;
    }
     public void deleteSensor(int i){
         String selectSQL ="DELETE FROM sensor WHERE sensor_id=?";;
         PreparedStatement preparedStatement;
         try {
             preparedStatement = dbConnection.prepareStatement(selectSQL);
             preparedStatement.setInt(1, i);
             preparedStatement.executeUpdate();
         } catch (SQLException ex) {
             Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
         }
           
         
     }
    public Sensor getSensorByID(int sensorID){
        Sensor sensor=null;
        try {    
            String selectSQL = "SELECT * FROM sensor WHERE sensor_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, sensorID);
            preparedStatement.execute();
            ResultSet rs=preparedStatement.getResultSet();
            if(rs.first()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int location_id=rs.getInt("location_id");
                int vendor_id=rs.getInt("vendor_id");
                sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                sensor.setLocationId(location_id);
                sensor.setVendorId(vendor_id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return sensor;
    }
    public int UpdateSensorLocation(Sensor sensor,Location location){
        String selectSQL ="UPDATE sensor SET location_id=?,sensor_status='R' WHERE sensor_id=?";;
        PreparedStatement preparedStatement;
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, location.getLocationId());
            preparedStatement.setInt(2, sensor.getSensorId());
            return preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    public int removeSensorFromLocation(Sensor sensor){
        String selectSQL ="UPDATE sensor SET location_id=NULL WHERE sensor_id=?";;
        PreparedStatement preparedStatement;
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, sensor.getSensorId());
            return preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    public ArrayList<Sensor> getSensorStatus(){
        ArrayList<Sensor> sensors=new ArrayList<Sensor>();
        String selectSQL="SELECT * FROM sensor WHERE sensor_status='E'";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery(selectSQL);
            while (rs.next()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int location_id=rs.getInt("location_id");
                int vendor_id=rs.getInt("vendor_id");
                Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
         sensor.setLocationId(location_id);
                sensor.setVendorId(vendor_id);
                sensors.add(sensor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensors;
    }
    public ArrayList<Sensor> getSensorExpired(){
        ArrayList<Sensor> sensors=new ArrayList<Sensor>();
        String selectSQL="SELECT * FROM sensor WHERE expected_expiry_date < ?";
        PreparedStatement preparedStatement;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date utilDate = cal.getTime();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setDate(1, sqlDate);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int location_id=rs.getInt("location_id");
                int vendor_id=rs.getInt("vendor_id");
                Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                sensor.setLocationId(location_id);
                sensor.setVendorId(vendor_id);
                sensors.add(sensor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensors;
    }
    public ArrayList<Sensor> getSensorLastCheck(){
        ArrayList<Sensor> sensors=new ArrayList<Sensor>();
        String selectSQL="SELECT * FROM sensor WHERE DATEDIFF(?,last_check_date)>14";
        PreparedStatement preparedStatement;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        java.util.Date utilDate = cal.getTime();
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setDate(1, sqlDate);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int location_id=rs.getInt("location_id");
                int vendor_id=rs.getInt("vendor_id");
                Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                sensor.setLocationId(location_id);
                sensor.setVendorId(vendor_id);
                sensors.add(sensor);
            }
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensors;
    }
    public void updateSensor(Sensor sensor){
        try {
            String updateSQL = "UPDATE sensor SET sensor_status =?, location_id=? WHERE sensor_id =?  ";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL);
            preparedStatement.setString(1, sensor.getSensorStatus());
            preparedStatement.setInt(2, sensor.getLocationId());
            preparedStatement.setInt(3, sensor.getSensorId());
            
            preparedStatement.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(vendorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public Sensor getSensorDetails1(int id) throws SQLException{
       
        String selectSQL = "SELECT * FROM sensor   WHERE sensor_id=?";
         PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.first()) {
                int sensorId = rs.getInt("sensor_id");
                String sensorType = rs.getString("sensor_type");
                String sensorStatus = rs.getString("sensor_status");
                Date dateInstalled = rs.getTimestamp("date_installed");
                Date expectedExpiryDate = rs.getTimestamp("expected_expiry_date");
                Date lastCheckDate = rs.getTimestamp("last_check_date");
                int locationId = rs.getInt("location_id");
                int vendorId = rs.getInt("vendor_id");
                 Sensor sensor = new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,lastCheckDate,expectedExpiryDate);
                 sensor.setLocationId(locationId);
                 sensor.setVendorId(vendorId);
                 return sensor;
        }
        return null;
    }
}
