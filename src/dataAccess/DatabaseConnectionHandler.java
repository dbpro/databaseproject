/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author kanchanaR
 */
public class DatabaseConnectionHandler {
    private static Connection con = null;

    /**
     * method returns a connection to the database if exists
     *
     * @return database connection
     */
    public static Connection createConnection() {
        try {
            Class.forName(Constants.DRIVER_NAME);
            Connection conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USERNAME, Constants.DB_PASSWORD);
            return conn;
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;

    }

    /**
     * method gives the connection to the database if exists else get a connection
     *
     * @return database connection
     */
    public static Connection getConnection() {
        if (con == null) {
            con = createConnection();
            return con;
        }
        try {
            if (con.isValid(1)) {
                return con;
            }
        } catch (SQLException ex) {

        }
        closeConnection();
        con = createConnection();
        return con;
    }

    public static void closeConnection() {
        try {
            con.close();
        } catch (Exception ex) {

        }
        con = null;
    }
}
