/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import businessLogic.user.User;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author chath
 */
@Entity
@Table(name = "maintainance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Maintainance.findAll", query = "SELECT m FROM Maintainance m"),
    @NamedQuery(name = "Maintainance.findByUsrId", query = "SELECT m FROM Maintainance m WHERE m.usrId = :usrId"),
    @NamedQuery(name = "Maintainance.findByWorkType", query = "SELECT m FROM Maintainance m WHERE m.workType = :workType")})
public class Maintainance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "usr_id")
    private Integer usrId;
    @Column(name = "work_type")
    private String workType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "maintainance")
    private List<Maintains> maintainsList;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private User usertb;

    public Maintainance() {
    }

    public Maintainance(Integer usrId,String work) {
        this.usrId = usrId;
        this.workType=work;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    @XmlTransient
    public List<Maintains> getMaintainsList() {
        return maintainsList;
    }

    public void setMaintainsList(List<Maintains> maintainsList) {
        this.maintainsList = maintainsList;
    }

    public User getUsertb() {
        return usertb;
    }

    public void setUsertb(User usertb) {
        this.usertb = usertb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usrId != null ? usrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Maintainance)) {
            return false;
        }
        Maintainance other = (Maintainance) object;
        if ((this.usrId == null && other.usrId != null) || (this.usrId != null && !this.usrId.equals(other.usrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Maintainance[ usrId=" + usrId + " ]";
    }
    
}
