/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewLayer.user;

import businessLogic.user.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chath
 */
public class UserTM extends AbstractTableModel {

    private final String[] columnNames = new String[]{
        "<html>First Name</html>", "<html>Last Name</html>", "<html>NIC</html>", "<html>Department</html>", "<html>Street</html>", "<html>Area</html>"
    };
    private static final int INDEX_FIST_NAME = 0;
    private static final int INDEX_LAST_NAME = 1;
    private static final int INDEX_NIC = 2;
    private static final int INDEX_DEPARTMENT = 3;
    private static final int INDEX_STREET = 4;
    private static final int INDEX_AREA = 5;
    ArrayList<User> users;
    ArrayList<User> usersByType;
    ArrayList<User> list;

    public UserTM(ArrayList<User> list) {
        users = list;
        this.list = list;
    }

    @Override
    public int getRowCount() {
        return users.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User c = users.get(rowIndex);
        Object returnVal = null;
        switch (columnIndex) {
            case INDEX_FIST_NAME:
                returnVal = c.getFirstName();
                break;
            case INDEX_LAST_NAME:
                returnVal = c.getLastName();
                break;
            case INDEX_NIC:
                returnVal = c.getNic();
                break;
            case INDEX_AREA:
                returnVal = c.getArea();
                break;
            case INDEX_STREET:
                returnVal = c.getStreet();
                break;
            case INDEX_DEPARTMENT:
                returnVal = c.getDepartment();
                break;
            default:
                break;
        }
        return returnVal;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (users == null || users.isEmpty()) {
            return Object.class;
        }
        return getValueAt(0, columnIndex).getClass();
    }

    public void setList(ArrayList<User> list) {
        this.list = list;
        fireTableDataChanged();
    }

    public User getUserAt(int index) {
        if (index < users.size()) {
            return users.get(index);
        }
        return null;
    }
    public void filter(String userType, int searchBy, String query) {
        if (userType != null) {
            usersByType = new ArrayList<>();
            for (User u : list) {
                if (u.getAccessLevel().equals(userType)) {
                    usersByType.add(u);
                }
            }
        } else {
            usersByType = list;
        }
        if (query.equals("")) {
            users = usersByType;
        } else if (searchBy == 0) {
            users = new ArrayList<>();
            usersByType.stream().filter((u) -> (u.getFirstName().contains(query) || u.getLastName().contains(query))).forEach((u) -> {
                users.add(u);
            });
        } else if (searchBy == 1) {
            users = new ArrayList<>();
            usersByType.stream().filter((u) -> (u.getNic().startsWith(query))).forEach((u) -> {
                users.add(u);
            });
        } else {
            users = new ArrayList<>();
            usersByType.stream().filter((u) -> (u.getStreet().contains(query) || u.getArea().contains(query))).forEach((u) -> {
                users.add(u);
            });
        }
        fireTableDataChanged();
    }
}
