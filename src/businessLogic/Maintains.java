/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import businessLogic.Maintainance;
import businessLogic.Sensor;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author chath
 */
@Entity
@Table(name = "maintains")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Maintains.findAll", query = "SELECT m FROM Maintains m"),
    @NamedQuery(name = "Maintains.findByUsrId", query = "SELECT m FROM Maintains m WHERE m.maintainsPK.usrId = :usrId"),
    @NamedQuery(name = "Maintains.findBySensorId", query = "SELECT m FROM Maintains m WHERE m.maintainsPK.sensorId = :sensorId"),
    @NamedQuery(name = "Maintains.findByRepDate", query = "SELECT m FROM Maintains m WHERE m.maintainsPK.repDate = :repDate"),
    @NamedQuery(name = "Maintains.findByRepTime", query = "SELECT m FROM Maintains m WHERE m.maintainsPK.repTime = :repTime"),
    @NamedQuery(name = "Maintains.findByDescription", query = "SELECT m FROM Maintains m WHERE m.description = :description")})
public class Maintains implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MaintainsPK maintainsPK;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Maintainance maintainance;
    @JoinColumn(name = "sensor_id", referencedColumnName = "sensor_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sensor sensor;
    @Basic(optional = false)
    @Column(name = "usr_id")
    private int usrId;
    @Basic(optional = false)
    @Column(name = "sensor_id")
    private int sensorId;
    @Basic(optional = false)
    @Column(name = "rep_date")
    @Temporal(TemporalType.DATE)
    private Date repDate;
    @Basic(optional = false)
    @Column(name = "rep_time")
    @Temporal(TemporalType.TIME)
    private Time repTime;

    public Maintains() {
    }

    public Maintains(int usrId, int sensorId, Date repDate, Time repTime, String desc) {
        this.usrId = usrId;
        this.sensorId = sensorId;
        this.repDate = repDate;
        this.repTime = repTime;
        this.description = desc;
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public Date getRepDate() {
        return repDate;
    }

    public void setRepDate(Date repDate) {
        this.repDate = repDate;
    }

    public Time getRepTime() {
        return repTime;
    }

    public void setRepTime(Time repTime) {
        this.repTime = repTime;
    }

    public Maintains(MaintainsPK maintainsPK) {
        this.maintainsPK = maintainsPK;
    }

    public Maintains(MaintainsPK maintainsPK, String description) {
        this.maintainsPK = maintainsPK;
        this.description = description;
    }

    public Maintains(int usrId, int sensorId, Date repDate, Date repTime) {
        this.maintainsPK = new MaintainsPK(usrId, sensorId, repDate, repTime);
    }

    public MaintainsPK getMaintainsPK() {
        return maintainsPK;
    }

    public void setMaintainsPK(MaintainsPK maintainsPK) {
        this.maintainsPK = maintainsPK;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Maintainance getMaintainance() {
        return maintainance;
    }

    public void setMaintainance(Maintainance maintainance) {
        this.maintainance = maintainance;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maintainsPK != null ? maintainsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Maintains)) {
            return false;
        }
        Maintains other = (Maintains) object;
        if ((this.maintainsPK == null && other.maintainsPK != null) || (this.maintainsPK != null && !this.maintainsPK.equals(other.maintainsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Maintains[ maintainsPK=" + maintainsPK + " ]";
    }

}
