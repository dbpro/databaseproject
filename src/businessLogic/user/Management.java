/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import dataAccess.user.UserDA;

/**
 *
 * @author ChathurangaKCD
 */
public class Management extends User {

    private String designation;

    public Management() {
    }

    public Management(Integer usrId, String firstName, String lastName, String nic, String username, String password, String accessLevel, String departmet, String street, String area) {
        super(usrId, firstName, lastName, nic, username, password, accessLevel, departmet, street, area);
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public void refresh() {
        UserDA userDA = new UserDA();
        userDA.refreshUser(this);
        userDA.refreshManagementDetails(this);
        userDA.refreshContactDetails(this);
    }

    @Override
    public void add() {
        UserDA userDA = new UserDA();
        userDA.begin();
        userDA.addUser(this);
        userDA.addManagement(this);
        userDA.setContactDetails(this);
        userDA.close();
    }

    @Override
    public void update() {
        UserDA userDA = new UserDA();
        userDA.begin();
        userDA.editUser(this);
        userDA.setManagementDetails(this);
        userDA.setContactDetails(this);
        userDA.close();
    }

    @Override
    public String getAccessLevel() {
        return UserHandler.ACCESS_MANAGEMENT;
    }
}
