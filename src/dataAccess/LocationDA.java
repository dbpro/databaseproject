/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import businessLogic.Location;
import businessLogic.Sensor;
import java.sql.Date;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kanchanaR
 */

public class LocationDA {
    private Connection dbConnection;
    public LocationDA(){
        dbConnection=DatabaseConnectionHandler.createConnection();
    }
    public boolean insertLocation(Location location) throws SQLException{
        String insertSQL = "INSERT INTO location(location_name,street,area,latitude,longitude) VALUES(?,?,?,?,?)";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL);
        preparedStatement.setString(1, location.getLocationName());
        preparedStatement.setString(2, location.getStreet());
        preparedStatement.setString(3, location.getArea());
        preparedStatement.setString(4, location.getLatitude());
        preparedStatement.setString(5, location.getLongitude());
        preparedStatement.execute();
        if(getLocationByName(location.getLocationName())!=null){
            return true;
        }
        return false;
    }
    public List<Location> getLocationList(){
        ArrayList<Location> loc=new ArrayList<Location>();
        try {
            
            String selectSQL = "SELECT * FROM location WHERE location_id>0";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int locationid = rs.getInt("location_id");
                String locatonname = rs.getString("location_name");
                String street=rs.getString("street");
                String area=rs.getString("area");
                String latitude=rs.getString("latitude");
                String longitude=rs.getString("longitude");
                Location location=new Location(locationid,locatonname,latitude,longitude);
                location.setStreet(street);
                location.setArea(area);
                loc.add(location);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return loc;
    }
    public Location getLocationByID(int id) throws SQLException{
        
        String selectSQL = "SELECT * FROM location WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.first()) {
                int locationid = rs.getInt("location_id");
                String locatonname = rs.getString("location_name");
                String street=rs.getString("street");
                String area=rs.getString("area");
                String latitude=rs.getString("latitude");
                String longitude=rs.getString("longitude");
                Location location=new Location(locationid,locatonname,latitude,longitude);
                location.setStreet(street);
                location.setArea(area);
                return location;
        }
        return null;
    }
    public Location getLocationByName(String name){
        
        try {
            String selectSQL = "SELECT * FROM location WHERE location_name=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.first()) {
                int locationid = rs.getInt("location_id");
                String locatonname = rs.getString("location_name");
                String street=rs.getString("street");
                String area=rs.getString("area");
                String latitude=rs.getString("latitude");
                String longitude=rs.getString("longitude");
                Location location=new Location(locationid,locatonname,latitude,longitude);
                location.setStreet(street);
                location.setArea(area);
                return location;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Location> getLocationListByName(String locationname){
        try {
            ArrayList<Location> loc=new ArrayList<Location>();
            String selectSQL = "SELECT * FROM location WHERE location_name LIKE ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setString(1, "%"+locationname+"%");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int locationid = rs.getInt("location_id");
                String locatonname = rs.getString("location_name");
                String street=rs.getString("street");
                String area=rs.getString("area");
                String latitude=rs.getString("latitude");
                String longitude=rs.getString("longitude");
                Location location=new Location(locationid,locatonname,latitude,longitude);
                location.setStreet(street);
                location.setArea(area);
                loc.add(location);
            }
            return loc;
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public int updateLocation(Location location) throws SQLException{
        String updateSQL = "UPDATE location set location_name=?,street=?,area=?,latitude=?,longitude=? WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL);
        preparedStatement.setString(1, location.getLocationName());
        preparedStatement.setString(2, location.getStreet());
        preparedStatement.setString(3, location.getArea());
        preparedStatement.setString(4, location.getLatitude());
        preparedStatement.setString(5, location.getLongitude());
        preparedStatement.setString(6, location.getLocationId().toString());
        return preparedStatement.executeUpdate();
    }
    public List<Sensor> getSensorByLocation(Location location){
        List<Sensor> sensorList=new ArrayList<>();
        try {
            String selectSQL = "SELECT * FROM sensor WHERE location_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, location.getLocationId());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next())  {              
                int sensorId=rs.getInt("sensor_id");
                String sensorType=rs.getString("sensor_type");
                String sensorStatus=rs.getString("sensor_status");
                Date dateInstalled=rs.getDate("date_installed");
                Date lastCheckDate=rs.getDate("last_check_date");
                Date expectedExpiryDate=rs.getDate("expected_expiry_date");
                Sensor sensor=new Sensor(sensorId,sensorType,sensorStatus,dateInstalled,expectedExpiryDate);
                sensor.setLastCheckDate(lastCheckDate);
                sensorList.add(sensor);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensorList;
    }
}
