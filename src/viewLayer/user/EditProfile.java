/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewLayer.user;

import businessLogic.user.UserHandler;
import businessLogic.user.Administrator;
import businessLogic.user.InputValidator;
import businessLogic.user.Maintainance;
import businessLogic.user.Management;
import businessLogic.user.User;
import dataAccess.user.LoginDA;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JOptionPane;

/**
 *
 * @author chath
 */
public class EditProfile extends javax.swing.JPanel {

    public boolean editable;
    private User user;
    private DefaultListModel list;
    private userUI window;

    /**
     * Creates new form editUser
     */
    public EditProfile(User user, userUI ui) {
        this.editable = true;
        this.window = ui;
        initComponents();
        this.user = user;
        if (!UserHandler.getInstance().getLoggedInUser().equals(user)) {
            panel_edit.setVisible(false);
        }
        user.refresh();
        accessLevel.setEditable(false);
        System.out.println("." + user.getAccessLevel() + ".");
        switch (user.getAccessLevel()) {
            case UserHandler.ACCESS_MAINTAINANCE:
                panel_email.setVisible(false);
                panel_desg.setVisible(false);
                t_work.setText(((Maintainance) user).getWorkType());
                accessLevel.setSelectedIndex(2);
                break;
            case UserHandler.ACCESS_MANAGEMENT:
                panel_work.setVisible(false);
                panel_email.setVisible(false);
                t_desg.setText(((Management) user).getDesignation());
                accessLevel.setSelectedIndex(1);
                break;
            case UserHandler.ACCESS_ADMIN:
                panel_work.setVisible(false);
                Administrator a = (Administrator) user;
                t_desg.setText(a.getDesignation());
                t_email.setText(a.getEmail());
                accessLevel.setSelectedIndex(0);
                break;
        }
        this.t_NIC.setText(user.getNic());
        this.t_department.setText(user.getDepartment());
        this.t_area.setText(user.getArea());
        this.t_street.setText(user.getStreet());
        this.t_first.setText(user.getFirstName());
        this.t_last.setText(user.getLastName());
        for (String t : user.getContactNoList()) {
            this.list.addElement(t);
        }
        addComponents();
        toggleEditable();
    }
//<editor-fold defaultstate="collapsed" desc="editable component list">
    private LinkedList<JComponent> components;

    private void addComponents() {
        components = new LinkedList<>();
        components.add(this.t_NIC);
        components.add(this.t_addContact);
        components.add(this.t_area);
        components.add(this.t_contactNos);
        components.add(this.t_department);
        components.add(this.t_desg);
        components.add(this.t_email);
        components.add(this.t_first);
        components.add(this.t_last);
        components.add(this.t_street);
        components.add(this.t_work);
        components.add(this.btn_add);
        components.add(this.btn_remove);
    }
//</editor-fold>

    private void toggleEditable() {
        editable = !editable;
        for (JComponent j : components) {
            j.setEnabled(editable);
        }
        btn_discard.setVisible(editable);
        btn_edit_save.setText(editable ? "Save Changes" : "Edit");
        btn_loginDetails.setEnabled(!editable);
    }

    private boolean validateInputs() {
        if (!InputValidator.isValidNIC(t_NIC.getText())) {
            JOptionPane.showMessageDialog(this, new String[]{"Please enter a valid NIC."}, "Error  ", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (UserHandler.getInstance().getLoggedInUser().getAccessLevel().equals(UserHandler.ACCESS_ADMIN) || !InputValidator.isValidEmail(t_email.getText())) {
            JOptionPane.showMessageDialog(this, new String[]{"Please enter a valid email."}, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (InputValidator.isValidString(t_first.getText()) && InputValidator.isValidString(t_last.getText()) && InputValidator.isValidString(t_department.getText())) {

        } else {
            JOptionPane.showMessageDialog(this, new String[]{"Please check inpus."}, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if (new LoginDA().getUserBy(t_NIC.getText()) > 0) {
            JOptionPane.showMessageDialog(this, new String[]{"The NIC number you entered belongs to another user"}, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    public boolean isValidPhoneNumber(String no) {
        return true;
    }

    public boolean saveData() {
        if (validateInputs()) {
            ArrayList<String> l = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                l.add((String) list.getElementAt(i));
            }
            switch (user.getAccessLevel()) {
                case UserHandler.ACCESS_ADMIN:
                    Administrator admin = new Administrator(user.getUsrId(), t_first.getText(), t_last.getText(), t_NIC.getText(), UserHandler.USER, UserHandler.PASSWORD, user.getAccessLevel(), t_department.getText(), t_street.getText(), t_area.getText());
                    admin.setDesignation(t_desg.getText());
                    admin.setEmail(t_email.getText());
                    admin.setContactNoList(l);
                    admin.update();
                    break;
                case UserHandler.ACCESS_MANAGEMENT:
                    Management mng = new Management(user.getUsrId(), t_first.getText(), t_last.getText(), t_NIC.getText(), UserHandler.USER, UserHandler.PASSWORD, user.getAccessLevel(), t_department.getText(), t_street.getText(), t_area.getText());
                    mng.setDesignation(t_desg.getText());
                    mng.setContactNoList(l);
                    mng.update();
                    break;
                case UserHandler.ACCESS_MAINTAINANCE:
                    Maintainance mnt = new Maintainance(user.getUsrId(), t_first.getText(), t_last.getText(), t_NIC.getText(), UserHandler.USER, UserHandler.PASSWORD, user.getAccessLevel(), t_department.getText(), t_street.getText(), t_area.getText());
                    mnt.setWorkType(t_work.getText());
                    mnt.setContactNoList(l);
                    mnt.update();
                    break;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        t_street = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        t_last = new javax.swing.JTextField();
        t_department = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        t_area = new javax.swing.JTextField();
        t_first = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        t_NIC = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        accessLevel = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        panel_email = new javax.swing.JPanel();
        t_email = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        panel_desg = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        t_desg = new javax.swing.JTextField();
        panel_work = new javax.swing.JPanel();
        t_work = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        t_addContact = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        list=new DefaultListModel<String>();
        t_contactNos = new javax.swing.JList(list);
        btn_remove = new javax.swing.JButton();
        btn_add = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        panel_edit = new javax.swing.JPanel();
        btn_loginDetails = new javax.swing.JButton();
        btn_edit_save = new javax.swing.JButton();
        btn_discard = new javax.swing.JButton();
        lblRec = new javax.swing.JLabel();

        jLabel6.setText("Street");

        jLabel7.setText("Area");

        jLabel2.setText("Last name");

        jLabel3.setText("NIC");

        jLabel5.setText("Department");

        jLabel1.setText("First name");

        jLabel8.setText("Access Level");

        accessLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Administrator", "Management", "Maintainance" }));
        accessLevel.setEnabled(false);
        accessLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accessLevelActionPerformed(evt);
            }
        });

        t_email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                t_emailActionPerformed(evt);
            }
        });

        jLabel10.setText("Email");

        javax.swing.GroupLayout panel_emailLayout = new javax.swing.GroupLayout(panel_email);
        panel_email.setLayout(panel_emailLayout);
        panel_emailLayout.setHorizontalGroup(
            panel_emailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_emailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
                .addComponent(t_email, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panel_emailLayout.setVerticalGroup(
            panel_emailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_emailLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_emailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(t_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addContainerGap())
        );

        jLabel11.setText("Designation");

        javax.swing.GroupLayout panel_desgLayout = new javax.swing.GroupLayout(panel_desg);
        panel_desg.setLayout(panel_desgLayout);
        panel_desgLayout.setHorizontalGroup(
            panel_desgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_desgLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(t_desg, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                .addGap(36, 36, 36))
        );
        panel_desgLayout.setVerticalGroup(
            panel_desgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_desgLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_desgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(t_desg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addContainerGap())
        );

        jLabel4.setText("Work type");

        javax.swing.GroupLayout panel_workLayout = new javax.swing.GroupLayout(panel_work);
        panel_work.setLayout(panel_workLayout);
        panel_workLayout.setHorizontalGroup(
            panel_workLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_workLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(t_work, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panel_workLayout.setVerticalGroup(
            panel_workLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_workLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_workLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(t_work, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panel_desg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panel_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panel_work, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel_desg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel_work, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        t_contactNos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(t_contactNos);

        btn_remove.setText("Remove");
        btn_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_removeActionPerformed(evt);
            }
        });

        btn_add.setText("Add");
        btn_add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_addActionPerformed(evt);
            }
        });

        jLabel9.setText("Contact No.");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(t_addContact)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btn_add, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(btn_remove)
                        .addGap(47, 47, 47)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_remove)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(t_addContact, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_add))
                .addContainerGap())
        );

        btn_loginDetails.setText("Edit login details");
        btn_loginDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_loginDetailsActionPerformed(evt);
            }
        });

        btn_edit_save.setText("Edit");
        btn_edit_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_edit_saveActionPerformed(evt);
            }
        });

        btn_discard.setText("Discard Changes");
        btn_discard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_discardActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel_editLayout = new javax.swing.GroupLayout(panel_edit);
        panel_edit.setLayout(panel_editLayout);
        panel_editLayout.setHorizontalGroup(
            panel_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_editLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_loginDetails)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 212, Short.MAX_VALUE)
                .addComponent(btn_discard)
                .addGap(72, 72, 72)
                .addComponent(btn_edit_save)
                .addGap(18, 18, 18))
        );
        panel_editLayout.setVerticalGroup(
            panel_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_editLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_editLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_loginDetails)
                    .addComponent(btn_edit_save)
                    .addComponent(btn_discard))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(52, 52, 52)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(t_NIC, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(t_last, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(t_first))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(t_area))
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 27, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(t_street, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(t_department, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                                    .addComponent(accessLevel, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 48, Short.MAX_VALUE)
                .addComponent(panel_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(t_first, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(accessLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(t_department, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(t_street, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(t_area, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(t_last, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(t_NIC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel_edit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblRec.setBackground(new java.awt.Color(204, 204, 204));
        lblRec.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblRec.setText("User Details");
        lblRec.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        lblRec.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblRec, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRec)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void t_emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_t_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_t_emailActionPerformed

    private void btn_edit_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_edit_saveActionPerformed
        if (editable) {
            if (saveData()) {
                window.setButtons(true);
            } else {
                return;
            }
        } else {
            window.setButtons(false);
        }
        toggleEditable();
    }//GEN-LAST:event_btn_edit_saveActionPerformed

    private void accessLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accessLevelActionPerformed
//        switch (accessLevel.getSelectedItem().toString().substring(0, 1)) {
//            case "M":
//                panel_email.setVisible(false);
//                panel_work.setVisible(false);
//                break;
//            case "A":
//                panel_work.setVisible(false);
//                break;
//            case "U":
//                panel_desg.setVisible(false);
//                panel_email.setVisible(false);
//                break;
//        }
    }//GEN-LAST:event_accessLevelActionPerformed

    private void btn_addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_addActionPerformed
        String no = t_addContact.getText().trim();
        if (!isValidPhoneNumber(no)) {
            return;
        }
        for (int i = 0; i < list.getSize(); i++) {
            if (list.getElementAt(i).equals(no)) {
                JOptionPane.showMessageDialog(this, "The contact number you're trying add already exists.", "Already exists", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        t_addContact.setText("");
        list.addElement(no);
    }//GEN-LAST:event_btn_addActionPerformed

    private void btn_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_removeActionPerformed
        list.removeElement(t_contactNos.getSelectedValue());
    }//GEN-LAST:event_btn_removeActionPerformed

    private void btn_discardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_discardActionPerformed
        int proceed = JOptionPane.showConfirmDialog(this, "Discard changes?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (proceed == JOptionPane.YES_OPTION) {
            window.viewProfile();
        }
    }//GEN-LAST:event_btn_discardActionPerformed

    private void btn_loginDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_loginDetailsActionPerformed
        window.editLogin();
    }//GEN-LAST:event_btn_loginDetailsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox accessLevel;
    private javax.swing.JButton btn_add;
    private javax.swing.JButton btn_discard;
    private javax.swing.JButton btn_edit_save;
    private javax.swing.JButton btn_loginDetails;
    private javax.swing.JButton btn_remove;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblRec;
    private javax.swing.JPanel panel_desg;
    private javax.swing.JPanel panel_edit;
    private javax.swing.JPanel panel_email;
    private javax.swing.JPanel panel_work;
    private javax.swing.JTextField t_NIC;
    private javax.swing.JTextField t_addContact;
    private javax.swing.JTextField t_area;
    private javax.swing.JList t_contactNos;
    private javax.swing.JTextField t_department;
    private javax.swing.JTextField t_desg;
    private javax.swing.JTextField t_email;
    private javax.swing.JTextField t_first;
    private javax.swing.JTextField t_last;
    private javax.swing.JTextField t_street;
    private javax.swing.JTextField t_work;
    // End of variables declaration//GEN-END:variables
}
