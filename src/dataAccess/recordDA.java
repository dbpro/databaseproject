/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import businessLogic.*;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author kanchanaR
 */
public class recordDA {
    private Connection dbConnection;
    public recordDA(){
        dbConnection=DatabaseConnectionHandler.createConnection();
    }
    public List<WaterRecord> getWaterRecord(Location inlocation,java.sql.Date date1,java.sql.Date date2){
        try {
            ArrayList<WaterRecord> waterRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM waterview WHERE location_id=? AND record_date BETWEEN ? AND ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, inlocation.getLocationId());
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);
                
                int waterDepth=rs.getInt("water_depth");
                double phLevel=rs.getDouble("ph_level");
                double temperature=rs.getDouble("temperature");
                int turbidity=rs.getInt("turbidity");
                double pressure=rs.getDouble("pressure");
                double hardness=rs.getDouble("hardness");
                double bODvalue=rs.getDouble("BOD_value");
                double cODvalue=rs.getDouble("COD_value");
                double anionPer=rs.getDouble("anion_per");
                double cationPer=rs.getDouble("cation_per");
                double nitratePer=rs.getDouble("nitrate_per");
                WaterRecord water=new WaterRecord();
                water.setAnionPer(anionPer);
                water.setBODvalue(bODvalue);
                water.setCODvalue(cODvalue);
                water.setCationPer(cationPer);
                water.setHardness(hardness);
                water.setNitratePer(nitratePer);
                water.setPhLevel(phLevel);
                water.setPressure(pressure);
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setTemperature(temperature);
                water.setTurbidity(turbidity);
                water.setWaterDepth(waterDepth);
                waterRec.add(water);
            }
            return waterRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<WaterRecord> getWaterRecord(){
        try {
            ArrayList<WaterRecord> waterRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM waterview";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=new LocationDA().getLocationByID(locationid);
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);
                
                int waterDepth=rs.getInt("water_depth");
                double phLevel=rs.getDouble("ph_level");
                double temperature=rs.getDouble("temperature");
                int turbidity=rs.getInt("turbidity");
                double pressure=rs.getDouble("pressure");
                double hardness=rs.getDouble("hardness");
                double bODvalue=rs.getDouble("BOD_value");
                double cODvalue=rs.getDouble("COD_value");
                double anionPer=rs.getDouble("anion_per");
                double cationPer=rs.getDouble("cation_per");
                double nitratePer=rs.getDouble("nitrate_per");
                WaterRecord water=new WaterRecord();
                water.setAnionPer(anionPer);
                water.setBODvalue(bODvalue);
                water.setCODvalue(cODvalue);
                water.setCationPer(cationPer);
                water.setHardness(hardness);
                water.setNitratePer(nitratePer);
                water.setPhLevel(phLevel);
                water.setPressure(pressure);
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setTemperature(temperature);
                water.setTurbidity(turbidity);
                water.setWaterDepth(waterDepth);
                waterRec.add(water);
            }
            return waterRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<AirRecord> getAirRecord(Location inlocation,java.sql.Date date1,java.sql.Date date2){
        try {
            ArrayList<AirRecord> airRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM airview WHERE location_id=? AND record_date BETWEEN ? AND ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, inlocation.getLocationId());
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int altitude=rs.getInt("altitude");
                double humidityAbsolute=rs.getDouble("humidity_absolute");
                double oxygen=rs.getDouble("oxygen");
                double carbonDioxide=rs.getDouble("carbon_dioxide");
                double pollutantOrganic=rs.getDouble("pollutant_organic");
                double pollutantInorganic=rs.getDouble("pollutant_inorganic");
                float aerosol=rs.getFloat("aerosol");
                double temperature=rs.getDouble("temperature");
                float pressure=rs.getFloat("pressure"); 
                AirRecord air=new AirRecord();
                air.setAerosol(aerosol);
                air.setAltitude(altitude);
                air.setCarbonDioxide(carbonDioxide);
                air.setHumidityAbsolute(humidityAbsolute);
                air.setOxygen(oxygen);
                air.setPollutantInorganic(pollutantInorganic);
                air.setPollutantOrganic(pollutantOrganic);
                air.setPressure(pressure);
                air.setRecordId(recordid);
                air.setRecordtb(record);
                air.setTemperature(temperature);
                airRec.add(air);
            }
            return airRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<AirRecord> getAirRecord(){
        try {
            ArrayList<AirRecord> airRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM airview";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=new LocationDA().getLocationByID(locationid);
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int altitude=rs.getInt("altitude");
                double humidityAbsolute=rs.getDouble("humidity_absolute");
                double oxygen=rs.getDouble("oxygen");
                double carbonDioxide=rs.getDouble("carbon_dioxide");
                double pollutantOrganic=rs.getDouble("pollutant_organic");
                double pollutantInorganic=rs.getDouble("pollutant_inorganic");
                float aerosol=rs.getFloat("aerosol");
                double temperature=rs.getDouble("temperature");
                float pressure=rs.getFloat("pressure"); 
                AirRecord air=new AirRecord();
                air.setAerosol(aerosol);
                air.setAltitude(altitude);
                air.setCarbonDioxide(carbonDioxide);
                air.setHumidityAbsolute(humidityAbsolute);
                air.setOxygen(oxygen);
                air.setPollutantInorganic(pollutantInorganic);
                air.setPollutantOrganic(pollutantOrganic);
                air.setPressure(pressure);
                air.setRecordId(recordid);
                air.setRecordtb(record);
                air.setTemperature(temperature);
                airRec.add(air);
            }
            return airRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<WaterLvl> getWaterLvl(Location inlocation,java.sql.Date date1,java.sql.Date date2){
        try {
            ArrayList<WaterLvl> waterLvl=new ArrayList<>();
            String selectSQL = "SELECT * FROM waterlvlview WHERE location_id=? AND record_date BETWEEN ? AND ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, inlocation.getLocationId());
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);            
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int waterLevel=rs.getInt("water_level");
                WaterLvl water=new WaterLvl();
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setWaterLevel(waterLevel);
                waterLvl.add(water);
            }
            return waterLvl;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<WaterLvl> getWaterLvl(){
        try {
            ArrayList<WaterLvl> waterLvl=new ArrayList<>();
            String selectSQL = "SELECT * FROM waterlvlview";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=new LocationDA().getLocationByID(locationid);
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int waterLevel=rs.getInt("water_level");
                WaterLvl water=new WaterLvl();
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setWaterLevel(waterLevel);
                waterLvl.add(water);
            }
            return waterLvl;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<RainfallRecord> getRainRecord(Location inlocation,java.sql.Date date1,java.sql.Date date2){
        try {
            ArrayList<RainfallRecord> rainRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM rainfallview WHERE location_id=? AND record_date BETWEEN ? AND ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, inlocation.getLocationId());
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int rainfallLevel=rs.getInt("rainfall_level");
                double phLevel=rs.getDouble("ph_level");
                double chemical=rs.getDouble("chemical");
                double pollutant=rs.getDouble("pollutant");
                RainfallRecord rain=new RainfallRecord();
                rain.setRecordId(recordid);
                rain.setRecordtb(record);
                rain.setChemical(chemical);
                rain.setPhLevel(phLevel);
                rain.setPollutant(pollutant);
                rain.setRainfallLevel(rainfallLevel);
                rainRec.add(rain);
            }
            return rainRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<RainfallRecord> getRainRecord(){
        try {
            ArrayList<RainfallRecord> rainRec=new ArrayList<>();
            String selectSQL = "SELECT * FROM rainfallview";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=new LocationDA().getLocationByID(locationid);
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int rainfallLevel=rs.getInt("rainfall_level");
                double phLevel=rs.getDouble("ph_level");
                double chemical=rs.getDouble("chemical");
                double pollutant=rs.getDouble("pollutant");
                RainfallRecord rain=new RainfallRecord();
                rain.setRecordId(recordid);
                rain.setRecordtb(record);
                rain.setChemical(chemical);
                rain.setPhLevel(phLevel);
                rain.setPollutant(pollutant);
                rain.setRainfallLevel(rainfallLevel);
                rainRec.add(rain);
            }
            return rainRec;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Illuminance> getIlluminance(Location inlocation,java.sql.Date date1,java.sql.Date date2){
        try {
            ArrayList<Illuminance> illuminance=new ArrayList<>();
            String selectSQL = "SELECT * FROM  illuminanceview WHERE location_id=? AND record_date BETWEEN ? AND ?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, inlocation.getLocationId());
            preparedStatement.setDate(2, date1);
            preparedStatement.setDate(3, date2);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int sampleHeight=rs.getInt("sample_height");
                float luminacy=rs.getFloat("luminacy");
                Illuminance luminance=new Illuminance();
                luminance.setRecordId(recordid);
                luminance.setRecordtb(record);
                luminance.setLuminacy(luminacy);
                luminance.setSampleHeight(sampleHeight);
                illuminance.add(luminance);
            }
            return illuminance;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<Illuminance> getIlluminance(){
        try {
            ArrayList<Illuminance> illuminance=new ArrayList<>();
            String selectSQL = "SELECT * FROM illuminanceview";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=new LocationDA().getLocationByID(locationid);
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int sampleHeight=rs.getInt("sample_height");
                float luminacy=rs.getFloat("luminacy");
                Illuminance luminance=new Illuminance();
                luminance.setRecordId(recordid);
                luminance.setRecordtb(record);
                luminance.setLuminacy(luminacy);
                luminance.setSampleHeight(sampleHeight);
                illuminance.add(luminance);
            }
            return illuminance;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<WaterRecord> getWaterRecord(Location inlocation) throws SQLException{
        ArrayList<WaterRecord> waterRec=new ArrayList<>();
        String selectSQL = "SELECT * FROM waterview WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, inlocation.getLocationId());
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);
                
                int waterDepth=rs.getInt("water_depth");
                double phLevel=rs.getDouble("ph_level");
                double temperature=rs.getDouble("temperature");
                int turbidity=rs.getInt("turbidity");
                double pressure=rs.getDouble("pressure");
                double hardness=rs.getDouble("hardness");
                double bODvalue=rs.getDouble("BOD_value");
                double cODvalue=rs.getDouble("COD_value");
                double anionPer=rs.getDouble("anion_per");
                double cationPer=rs.getDouble("cation_per");
                double nitratePer=rs.getDouble("nitrate_per");
                WaterRecord water=new WaterRecord();
                water.setAnionPer(anionPer);
                water.setBODvalue(bODvalue);
                water.setCODvalue(cODvalue);
                water.setCationPer(cationPer);
                water.setHardness(hardness);
                water.setNitratePer(nitratePer);
                water.setPhLevel(phLevel);
                water.setPressure(pressure);
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setTemperature(temperature);
                water.setTurbidity(turbidity);
                water.setWaterDepth(waterDepth);
                waterRec.add(water);
        }
        return waterRec;
    }
    public List<AirRecord> getAirRecord(Location inlocation) throws SQLException{
        ArrayList<AirRecord> airRec=new ArrayList<>();
        String selectSQL = "SELECT * FROM airview WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, inlocation.getLocationId());
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int altitude=rs.getInt("altitude");
                double humidityAbsolute=rs.getDouble("humidity_absolute");
                double oxygen=rs.getDouble("oxygen");
                double carbonDioxide=rs.getDouble("carbon_dioxide");
                double pollutantOrganic=rs.getDouble("pollutant_organic");
                double pollutantInorganic=rs.getDouble("pollutant_inorganic");
                float aerosol=rs.getFloat("aerosol");
                double temperature=rs.getDouble("temperature");
                float pressure=rs.getFloat("pressure"); 
                AirRecord air=new AirRecord();
                air.setAerosol(aerosol);
                air.setAltitude(altitude);
                air.setCarbonDioxide(carbonDioxide);
                air.setHumidityAbsolute(humidityAbsolute);
                air.setOxygen(oxygen);
                air.setPollutantInorganic(pollutantInorganic);
                air.setPollutantOrganic(pollutantOrganic);
                air.setPressure(pressure);
                air.setRecordId(recordid);
                air.setRecordtb(record);
                air.setTemperature(temperature);
                airRec.add(air);
        }
        return airRec;
    }
    public List<WaterLvl> getWaterLvl(Location inlocation) throws SQLException{
        ArrayList<WaterLvl> waterLvl=new ArrayList<>();
        String selectSQL = "SELECT * FROM waterlvlview WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, inlocation.getLocationId());
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int waterLevel=rs.getInt("water_level");
                WaterLvl water=new WaterLvl();
                water.setRecordId(recordid);
                water.setRecordtb(record);
                water.setWaterLevel(waterLevel);
                waterLvl.add(water);
        }
        return waterLvl;
    }
    public List<RainfallRecord> getRainRecord(Location inlocation) throws SQLException{
        ArrayList<RainfallRecord> rainRec=new ArrayList<>();
        String selectSQL = "SELECT * FROM rainfallview WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, inlocation.getLocationId());
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int rainfallLevel=rs.getInt("rainfall_level");
                double phLevel=rs.getDouble("ph_level");
                double chemical=rs.getDouble("chemical");
                double pollutant=rs.getDouble("pollutant");
                RainfallRecord rain=new RainfallRecord();
                rain.setRecordId(recordid);
                rain.setRecordtb(record);
                rain.setChemical(chemical);
                rain.setPhLevel(phLevel);
                rain.setPollutant(pollutant);
                rain.setRainfallLevel(rainfallLevel);
                rainRec.add(rain);
        }
        return rainRec;
    }
    public List<Illuminance> getIlluminance(Location inlocation) throws SQLException{
        ArrayList<Illuminance> illuminance=new ArrayList<>();
        String selectSQL = "SELECT * FROM illuminanceview WHERE location_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, inlocation.getLocationId());
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
                int recordid=rs.getInt("record_id");
                int locationid = rs.getInt("location_id");
                Date date=rs.getDate("record_date");
                Time time=rs.getTime("record_time");
                Location location=inlocation;
                Recordtb record=new Recordtb();
                record.setRecordId(recordid);
                record.setRecordDate(date);
                record.setRecordTime(time);
                record.setLocationId(location);

                int sampleHeight=rs.getInt("sample_height");
                float luminacy=rs.getFloat("luminacy");
                Illuminance luminance=new Illuminance();
                luminance.setRecordId(recordid);
                luminance.setRecordtb(record);
                luminance.setLuminacy(luminacy);
                luminance.setSampleHeight(sampleHeight);
                illuminance.add(luminance);
        }
        return illuminance;
    }
}
