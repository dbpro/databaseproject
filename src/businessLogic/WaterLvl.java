/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "water_lvl")
@NamedQueries({
    @NamedQuery(name = "WaterLvl.findAll", query = "SELECT w FROM WaterLvl w")})
public class WaterLvl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    @Column(name = "water_level")
    private Integer waterLevel;
    @JoinColumn(name = "record_id", referencedColumnName = "record_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Recordtb recordtb;

    public WaterLvl() {
    }

    public WaterLvl(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(Integer waterLevel) {
        this.waterLevel = waterLevel;
    }

    public Recordtb getRecordtb() {
        return recordtb;
    }

    public void setRecordtb(Recordtb recordtb) {
        this.recordtb = recordtb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WaterLvl)) {
            return false;
        }
        WaterLvl other = (WaterLvl) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.WaterLvl[ recordId=" + recordId + " ]";
    }
    
}
