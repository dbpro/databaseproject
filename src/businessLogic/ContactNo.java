/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import businessLogic.user.User;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author chath
 */
@Entity
@Table(name = "contact_no")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContactNo.findAll", query = "SELECT c FROM ContactNo c"),
    @NamedQuery(name = "ContactNo.findByUsrId", query = "SELECT c FROM ContactNo c WHERE c.contactNoPK.usrId = :usrId"),
    @NamedQuery(name = "ContactNo.findByPhoneNo", query = "SELECT c FROM ContactNo c WHERE c.contactNoPK.phoneNo = :phoneNo")})
public class ContactNo implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ContactNoPK contactNoPK;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User usertb;

    public ContactNo() {
    }

    public ContactNo(ContactNoPK contactNoPK) {
        this.contactNoPK = contactNoPK;
    }

    public ContactNo(int usrId, String phoneNo) {
        this.contactNoPK = new ContactNoPK(usrId, phoneNo);
    }

    public ContactNoPK getContactNoPK() {
        return contactNoPK;
    }

    public void setContactNoPK(ContactNoPK contactNoPK) {
        this.contactNoPK = contactNoPK;
    }

    public User getUsertb() {
        return usertb;
    }

    public void setUsertb(User usertb) {
        this.usertb = usertb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contactNoPK != null ? contactNoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactNo)) {
            return false;
        }
        ContactNo other = (ContactNo) object;
        if ((this.contactNoPK == null && other.contactNoPK != null) || (this.contactNoPK != null && !this.contactNoPK.equals(other.contactNoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.ContactNo[ contactNoPK=" + contactNoPK + " ]";
    }
    
}
