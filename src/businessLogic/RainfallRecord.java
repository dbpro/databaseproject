/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "rainfall_record")
@NamedQueries({
    @NamedQuery(name = "RainfallRecord.findAll", query = "SELECT r FROM RainfallRecord r")})
public class RainfallRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    @Column(name = "rainfall_level")
    private Integer rainfallLevel;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ph_level")
    private double phLevel;
    @Column(name = "chemical")
    private double chemical;
    @Column(name = "pollutant")
    private double pollutant;
    @JoinColumn(name = "record_id", referencedColumnName = "record_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Recordtb recordtb;
   
    public RainfallRecord() {
    }

    public RainfallRecord(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRainfallLevel() {
        return rainfallLevel;
    }

    public void setRainfallLevel(Integer rainfallLevel) {
        this.rainfallLevel = rainfallLevel;
    }

    public double getPhLevel() {
        return phLevel;
    }

    public void setPhLevel(double phLevel) {
        this.phLevel = phLevel;
    }

    public double getChemical() {
        return chemical;
    }

    public void setChemical(double chemical) {
        this.chemical = chemical;
    }

    public double getPollutant() {
        return pollutant;
    }

    public void setPollutant(double pollutant) {
        this.pollutant = pollutant;
    }

    public Recordtb getRecordtb() {
        return recordtb;
    }

    public void setRecordtb(Recordtb recordtb) {
        this.recordtb = recordtb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RainfallRecord)) {
            return false;
        }
        RainfallRecord other = (RainfallRecord) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.RainfallRecord[ recordId=" + recordId + " ]";
    }
    
}
