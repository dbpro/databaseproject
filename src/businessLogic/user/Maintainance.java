/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import businessLogic.Maintains;
import dataAccess.user.UserDA;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ChathurangaKCD
 */
public class Maintainance extends User {

    private Integer usrId;
    private String workType;
    private ArrayList<Maintains> maintainsList;

    public Maintainance() {
    }

    public Maintainance(Integer usrId, String firstName, String lastName, String nic, String username, String password, String accessLevel, String departmet, String street, String area) {
        super(usrId, firstName, lastName, nic, username, password, accessLevel, departmet, street, area);
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public List<Maintains> getMaintainsList() {
        return maintainsList;
    }

    public void setMaintainsList(List<Maintains> maintainsList) {
        this.maintainsList = (ArrayList<Maintains>) maintainsList;
    }

    @Override
    public void refresh() {
        UserDA userDA = new UserDA();
        userDA.refreshUser(this);
        userDA.refreshMaintainanceDetails(this);
        userDA.refreshContactDetails(this);
    }

    @Override
    public void add() {
        UserDA userDA = new UserDA();
        userDA.begin();
        userDA.addUser(this);
        userDA.addMaintainance(this);
        userDA.setContactDetails(this);
        userDA.close();
    }

    @Override
    public void update() {
        UserDA userDA = new UserDA();
        userDA.begin();
        userDA.editUser(this);
        userDA.setMaintainanceDetails(this);
        userDA.setContactDetails(this);
        userDA.close();
    }

    @Override
    public String getAccessLevel() {
        return UserHandler.ACCESS_MAINTAINANCE;
    }
}
