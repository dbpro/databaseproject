/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author chath
 */
@Embeddable
public class MaintainsPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "usr_id")
    private int usrId;
    @Basic(optional = false)
    @Column(name = "sensor_id")
    private int sensorId;
    @Basic(optional = false)
    @Column(name = "rep_date")
    @Temporal(TemporalType.DATE)
    private Date repDate;
    @Basic(optional = false)
    @Column(name = "rep_time")
    @Temporal(TemporalType.TIME)
    private Date repTime;

    public MaintainsPK() {
    }

    public MaintainsPK(int usrId, int sensorId, Date repDate, Date repTime) {
        this.usrId = usrId;
        this.sensorId = sensorId;
        this.repDate = repDate;
        this.repTime = repTime;
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public Date getRepDate() {
        return repDate;
    }

    public void setRepDate(Date repDate) {
        this.repDate = repDate;
    }

    public Date getRepTime() {
        return repTime;
    }

    public void setRepTime(Date repTime) {
        this.repTime = repTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) usrId;
        hash += (int) sensorId;
        hash += (repDate != null ? repDate.hashCode() : 0);
        hash += (repTime != null ? repTime.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaintainsPK)) {
            return false;
        }
        MaintainsPK other = (MaintainsPK) object;
        if (this.usrId != other.usrId) {
            return false;
        }
        if (this.sensorId != other.sensorId) {
            return false;
        }
        if ((this.repDate == null && other.repDate != null) || (this.repDate != null && !this.repDate.equals(other.repDate))) {
            return false;
        }
        if ((this.repTime == null && other.repTime != null) || (this.repTime != null && !this.repTime.equals(other.repTime))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.MaintainsPK[ usrId=" + usrId + ", sensorId=" + sensorId + ", repDate=" + repDate + ", repTime=" + repTime + " ]";
    }
    
}
