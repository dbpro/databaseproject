/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t;

import businessLogic.ContactNo;
import businessLogic.Management;
import businessLogic.Administrator;
import businessLogic.Maintainance;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author chath
 */
@Entity
@Table(name = "usertb")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usertb.findAll", query = "SELECT u FROM Usertb u"),
    @NamedQuery(name = "Usertb.findByUsrId", query = "SELECT u FROM Usertb u WHERE u.usrId = :usrId"),
    @NamedQuery(name = "Usertb.findByFirstName", query = "SELECT u FROM Usertb u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "Usertb.findByLastName", query = "SELECT u FROM Usertb u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "Usertb.findByNic", query = "SELECT u FROM Usertb u WHERE u.nic = :nic"),
    @NamedQuery(name = "Usertb.findByUsername", query = "SELECT u FROM Usertb u WHERE u.username = :username"),
    @NamedQuery(name = "Usertb.findByPassword", query = "SELECT u FROM Usertb u WHERE u.password = :password"),
    @NamedQuery(name = "Usertb.findByAccessLevel", query = "SELECT u FROM Usertb u WHERE u.accessLevel = :accessLevel"),
    @NamedQuery(name = "Usertb.findByDepartment", query = "SELECT u FROM Usertb u WHERE u.department = :department"),
    @NamedQuery(name = "Usertb.findByStreet", query = "SELECT u FROM Usertb u WHERE u.street = :street"),
    @NamedQuery(name = "Usertb.findByArea", query = "SELECT u FROM Usertb u WHERE u.area = :area")})
public class User1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usr_id")
    private Integer usrId;
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;
    @Basic(optional = false)
    @Column(name = "NIC")
    private String nic;
    @Basic(optional = false)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "access_level")
    private String accessLevel;
    @Column(name = "department")
    private String department;
    @Column(name = "street")
    private String street;
    @Column(name = "area")
    private String area;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "usertb")
    private Administrator administrator;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "usertb")
    private Maintainance maintainance;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "usertb")
    private Management management;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usertb")
    private List<ContactNo> contactNoList;
    
    ///
    public static final String ACCESS_ADMIN="A";
    public static final String ACCESS_MANAGEMENT="M";
    public static final String ACCESS_USER="U";
    public static final String ACCESS_REMOVED="R";
    public static final String USER="%user%";
    public static final String PASSWORD="%pass%";

    public User1() {
    }

    public User1(Integer usrId) {
        this.usrId = usrId;
    }

    public User1(Integer usrId, String firstName, String lastName, String nic, String username, String password, String accessLevel) {
        this.usrId = usrId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nic = nic;
        this.username = username;
        this.password = password;
        this.accessLevel = accessLevel;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Administrator getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Administrator administrator) {
        this.administrator = administrator;
    }

    public Maintainance getMaintainance() {
        return maintainance;
    }

    public void setMaintainance(Maintainance maintainance) {
        this.maintainance = maintainance;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    @XmlTransient
    public List<ContactNo> getContactNoList() {
        return contactNoList;
    }

    public void setContactNoList(List<ContactNo> contactNoList) {
        this.contactNoList = contactNoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usrId != null ? usrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User1)) {
            return false;
        }
        User1 other = (User1) object;
        if ((this.usrId == null && other.usrId != null) || (this.usrId != null && !this.usrId.equals(other.usrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Usertb[ usrId=" + usrId + " ]";
    }
    
}
