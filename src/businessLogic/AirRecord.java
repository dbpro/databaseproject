/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "air_record")
@NamedQueries({
    @NamedQuery(name = "AirRecord.findAll", query = "SELECT a FROM AirRecord a")})
public class AirRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    @Column(name = "altitude")
    private Integer altitude;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "humidity_absolute")
    private double humidityAbsolute;
    @Column(name = "temperature")
    private double temperature;
    @Column(name = "pressure")
    private Float pressure;
    @Column(name = "oxygen")
    private double oxygen;
    @Column(name = "carbon_dioxide")
    private double carbonDioxide;
    @Column(name = "pollutant_organic")
    private double pollutantOrganic;
    @Column(name = "pollutant_inorganic")
    private double pollutantInorganic;
    @Column(name = "aerosol")
    private Float aerosol;
    @JoinColumn(name = "record_id", referencedColumnName = "record_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Recordtb recordtb;

    public AirRecord() {
    }

    public AirRecord(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public double getHumidityAbsolute() {
        return humidityAbsolute;
    }

    public void setHumidityAbsolute(double humidityAbsolute) {
        this.humidityAbsolute = humidityAbsolute;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public Float getPressure() {
        return pressure;
    }

    public void setPressure(Float pressure) {
        this.pressure = pressure;
    }

    public double getOxygen() {
        return oxygen;
    }

    public void setOxygen(double oxygen) {
        this.oxygen = oxygen;
    }

    public double getCarbonDioxide() {
        return carbonDioxide;
    }

    public void setCarbonDioxide(double carbonDioxide) {
        this.carbonDioxide = carbonDioxide;
    }

    public double getPollutantOrganic() {
        return pollutantOrganic;
    }

    public void setPollutantOrganic(double pollutantOrganic) {
        this.pollutantOrganic = pollutantOrganic;
    }

    public double getPollutantInorganic() {
        return pollutantInorganic;
    }

    public void setPollutantInorganic(double pollutantInorganic) {
        this.pollutantInorganic = pollutantInorganic;
    }

    public Float getAerosol() {
        return aerosol;
    }

    public void setAerosol(Float aerosol) {
        this.aerosol = aerosol;
    }

    public Recordtb getRecordtb() {
        return recordtb;
    }

    public void setRecordtb(Recordtb recordtb) {
        this.recordtb = recordtb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AirRecord)) {
            return false;
        }
        AirRecord other = (AirRecord) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.AirRecord[ recordId=" + recordId + " ]";
    }
    
}
