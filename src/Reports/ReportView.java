/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Reports;

import java.sql.Connection;
import java.awt.Container;
import java.util.HashMap;
import javax.swing.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Melanka Saroad
 */
public class ReportView extends JFrame{
    
     public ReportView(String fileName, String title)
    {
        this(fileName, title, null);
    }
    public ReportView(String fileName, String title, HashMap para)
    {
        super(title);
        Connection con = dataAccess.DatabaseConnectionHandler.getConnection();
        System.out.println(con);
        try
        {
            JasperPrint print = JasperFillManager.fillReport(fileName, para, con);
            JRViewer viewer = new JRViewer(print);
            Container c = this.getContentPane();
            c.add(viewer);
            System.out.println(print);
            System.out.println(viewer);
            System.out.println(c);
        } 
        catch (JRException jRException)
        {
            System.out.println(jRException);
        }
        setBounds(10, 10, 900, 700);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
