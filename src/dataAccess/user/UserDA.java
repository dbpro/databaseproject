/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess.user;

import businessLogic.user.Administrator;
import businessLogic.user.Maintainance;
import businessLogic.user.Management;
import businessLogic.user.User;
import dataAccess.DatabaseConnectionHandler;
import dataAccess.recordDA;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import businessLogic.user.UserHandler;

/**
 *
 * @author ChathurangaKCD
 */
public class UserDA {

    private final Connection dbConnection;

    public UserDA() {
        dbConnection = DatabaseConnectionHandler.createConnection();
    }

    //returns a list of all valid users.
    public ArrayList<User> getUsers(int id) {
        try {
            ArrayList<User> list = new ArrayList<>();
            String selectSQL = "SELECT * FROM usertb where not usr_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                User user;
                switch (rs.getString("access_level")) {
                    case UserHandler.ACCESS_ADMIN:
                        Administrator admin = new Administrator();
                        user = admin;
                        break;
                    case UserHandler.ACCESS_MAINTAINANCE:
                        Maintainance mnt = new Maintainance();
                        user = mnt;
                        break;
                    case UserHandler.ACCESS_MANAGEMENT:
                        Management mng = new Management();
                        user = mng;
                        break;
                    default:
                        continue;
                }
                user.setUsrId(rs.getInt("usr_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setNic(rs.getString("NIC"));
                user.setDepartment(rs.getString("department"));
                user.setStreet(rs.getString("street"));
                user.setArea(rs.getString("area"));
                list.add(user);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public User getUser(String username) {
        User user = new Administrator();
        user.setUsrId(0);
        try {
            String selectSQL = "SELECT access_level,password,usr_id FROM usertb where username=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setString(1, username);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                switch (rs.getString("access_level")) {
                    case UserHandler.ACCESS_ADMIN:
                        break;
                    case UserHandler.ACCESS_MAINTAINANCE:
                        Maintainance mnt = new Maintainance();
                        user = mnt;
                        break;
                    case UserHandler.ACCESS_MANAGEMENT:
                        Management mng = new Management();
                        user = mng;
                }
                user.setUsrId(rs.getInt("usr_id"));
                user.setUsername(username);
                user.setPassword(rs.getString("password"));
                return user;
            }
            return user;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
            user.setUsrId(-1);
            return user;
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

//<editor-fold defaultstate="collapsed" desc="Add user">
    public void addUser(User user) {
        try {
            String query = " insert into usertb (first_name, last_name, NIC, username,password,access_level,department,street,area)" + " values (?,?, ?, ?, ?, ?,?,?,?)";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getFirstName());
            preparedStmt.setString(2, user.getLastName());
            preparedStmt.setString(3, user.getNic());
            preparedStmt.setString(4, user.getUsername());
            preparedStmt.setString(5, user.getPassword());
            preparedStmt.setString(6, user.getAccessLevel());
            preparedStmt.setString(7, user.getDepartment());
            preparedStmt.setString(8, user.getStreet());
            preparedStmt.setString(9, user.getArea());
            preparedStmt.execute();
            query = "select usr_id from usertb where username=?";
            preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getUsername());
            ResultSet rs = preparedStmt.executeQuery();
            rs.next();
            int lastid = rs.getInt("usr_id");
            user.setUsrId(lastid);
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void addAdministrator(Administrator user) {
        try {
            String query = " insert into administrator (usr_id, designation,email)" + " values (?,?, ?)";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setInt(1, user.getUsrId());
            preparedStmt.setString(2, user.getDesignation());
            preparedStmt.setString(3, user.getEmail());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void addManagement(Management user) {
        try {
            String query = " insert into management (usr_id, designation,email)" + " values (?,?)";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setInt(1, user.getUsrId());
            preparedStmt.setString(2, user.getDesignation());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void addMaintainance(Maintainance user) {
        try {
            String query = " insert into maintainance (usr_id, work_type)" + " values (?,?)";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setInt(1, user.getUsrId());
            preparedStmt.setString(2, user.getWorkType());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="edit user">
    public void editUser(User user) {
        try {
            String query = "update usertb set first_name=? , last_name=? , NIC=?, department=? ,street=?,area=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getFirstName());
            preparedStmt.setString(2, user.getLastName());
            preparedStmt.setString(3, user.getNic());
            preparedStmt.setString(4, user.getDepartment());
            preparedStmt.setString(5, user.getStreet());
            preparedStmt.setString(6, user.getArea());
            preparedStmt.setInt(7, user.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void editLogin(User user) {
        try {
            String query = "update usertb set user_name=? , password=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getUsername());
            preparedStmt.setString(2, user.getPassword());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void setAdministratorDetails(Administrator admin) {
        try {
            String query = "update administrator set designation=?,email=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, admin.getDesignation());
            preparedStmt.setString(2, admin.getEmail());
            preparedStmt.setInt(3, admin.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void setManagementDetails(Management user) {
        try {
            String query = "update management set designation=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getDesignation());
            preparedStmt.setInt(2, user.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void setMaintainanceDetails(Maintainance user) {
        try {
            String query = "update maintainance set work_type=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getWorkType());
            preparedStmt.setInt(2, user.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void setContactDetails(User user) {
        try {
            String query = "delete from contact_no where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setInt(1, user.getUsrId());
            preparedStmt.execute();
            query = "insert into contact_no (usr_id,phone_no) values (?,?)";
            for (String no : user.getContactNoList()) {
                preparedStmt = dbConnection.prepareStatement(query);
                preparedStmt.setInt(1, user.getUsrId());
                preparedStmt.setString(2, no);
                preparedStmt.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="refresh user">
    public void refreshUser(User user) {
        try {
            String selectSQL = "SELECT * FROM usertb where usr_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, user.getUsrId());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user.setUsername(rs.getString("username"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setNic(rs.getString("NIC"));
                user.setDepartment(rs.getString("department"));
                user.setStreet(rs.getString("street"));
                user.setArea(rs.getString("area"));
                user.setPassword(rs.getString("password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            //          DatabaseConnectionHandler.closeConnection();
        }
    }

    public void refreshAdministratorDetails(Administrator user) {
        try {
            String selectSQL = "SELECT * FROM administrator where usr_id=?";
            PreparedStatement preparedStatement = DatabaseConnectionHandler.getConnection().prepareStatement(selectSQL);
            preparedStatement.setInt(1, user.getUsrId());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user.setDesignation(rs.getString("designation"));
                user.setEmail(rs.getString("email"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void refreshManagementDetails(Management user) {
        try {
            String selectSQL = "SELECT * FROM management where usr_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, user.getUsrId());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user.setDesignation(rs.getString("designation"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void refreshMaintainanceDetails(Maintainance user) {
        try {
            String selectSQL = "SELECT * FROM maintainance where usr_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, user.getUsrId());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                user.setWorkType(rs.getString("work_type"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void refreshContactDetails(User user) {
        try {
            String selectSQL = "SELECT phone_no FROM contact_no where usr_id=?";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, user.getUsrId());
            ResultSet rs = preparedStatement.executeQuery();
            user.setContactNoList(new ArrayList<>());
            while (rs.next()) {
                user.getContactNoList().add(rs.getString("phone_no"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

//</editor-fold>
    public void begin() {
        try {
            dbConnection.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(LoginDA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void close() {
        if (dbConnection != null) {
            try {
                dbConnection.commit();
                dbConnection.close();
            } catch (SQLException ex) {
                Logger.getLogger(LoginDA.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
//<editor-fold defaultstate="collapsed" desc="not needed">
//    public void setAccessLevel(User user) {
//        try {
//            String query = "update usertb set access_level=? where usr_id=?";
//            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
//            preparedStmt.setString(1, user.getAccessLevel());
//            preparedStmt.setInt(2, user.getUsrId());
//            preparedStmt.execute();
//            DatabaseConnectionHandler.closeConnection();
//        } catch (SQLException ex) {
//            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//</editor-fold>
}
