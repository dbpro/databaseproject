/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess;

import businessLogic.Sensor;
import businessLogic.Vendor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class vendorDA {

    private Connection dbConnection;

    public vendorDA() {
        dbConnection = DatabaseConnectionHandler.createConnection();
    }

    public void insertVendor(Vendor vendor) throws SQLException {
        String insertSQL1 = "INSERT INTO vendor(vendor_name,street,area) VALUES(?,?,?)";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL1);
        preparedStatement.setString(1, vendor.getVendorName());
        preparedStatement.setString(2, vendor.getStreet());
        preparedStatement.setString(3, vendor.getArea());
        preparedStatement.execute();
        insertVendorcontact(vendor);
    }

    public void insertVendorcontact(Vendor vendor) throws SQLException {

        String insertSQL1 = "INSERT INTO vendor_contact(vendor_id,phone_number) VALUES(?,?)";
        System.out.println(vendor.getTelephoneNum().size());
        if (vendor.getTelephoneNum().size() > 0) {
            for (int i = 0; i < vendor.getTelephoneNum().size(); i++) {
                PreparedStatement preparedStatement = dbConnection.prepareStatement(insertSQL1);

                preparedStatement.setInt(1, this.getLastVendorId());
                preparedStatement.setString(2, vendor.getTelephoneNum().get(i).toString());
                preparedStatement.execute();
            }
        }
    }
    public void updateVendor(Vendor vendor,List tele){
        try {
            String updateSQL = "UPDATE vendor SET vendor_name =?, street=?,area = ? WHERE vendor_id =?  ";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL);
            preparedStatement.setString(1, vendor.getVendorName());
            preparedStatement.setString(2, vendor.getStreet());
            preparedStatement.setString(3, vendor.getArea());
            preparedStatement.setInt(4, vendor.getVendorId());
            preparedStatement.executeUpdate();
            updateVendorcontact(vendor,tele);
        } catch (SQLException ex) {
            Logger.getLogger(vendorDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void updateVendorcontact(Vendor vendor,List tele){
        String updateSQL1 = "UPDATE vendor_contact SET  phone_number=?  WHERE vendor_id=? AND phone_number = ?";
        System.out.println(vendor.getTelephoneNum().size());
        if (vendor.getTelephoneNum().size() > 0) {
            for (int i = 0; i < vendor.getTelephoneNum().size(); i++) {
                try {
                    PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL1);
                    
                    preparedStatement.setInt(2, this.getLastVendorId());
                    preparedStatement.setString(1, vendor.getTelephoneNum().get(i).toString());
                    if(i==0){
                        preparedStatement.setString(3,tele.get(0)+"" );
                        System.out.println("helloooo");
                    }
                    if(i==1){
                       preparedStatement.setString(3,tele.get(1)+"" ); 
                        System.out.println("haaii");
                    }
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    Logger.getLogger(vendorDA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void deleteVendor(int i) {
        String selectSQL = "DELETE FROM vendor WHERE vendor_id=?";

        PreparedStatement preparedStatement;
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, i);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(sensorDA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
public Vendor getVendorByID(int id) throws SQLException {

        String selectSQL = "SELECT * FROM vendor WHERE vendor_id=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.first()) {
            System.out.println("hai");
            int vendorId = rs.getInt("vendor_id");
            String namev = rs.getString("vendor_name");
            String street = rs.getString("street");
            String area = rs.getString("area");
            ArrayList<String> telephoneNums = new ArrayList<String>();
            String selectSQL2 = "SELECT * FROM vendor_contact WHERE vendor_id =?";

            PreparedStatement preparedStatement2 = dbConnection.prepareStatement(selectSQL2);
            preparedStatement2.setInt(1, vendorId);
            ResultSet rs2 = preparedStatement2.executeQuery();
            
            while (rs2.next()) {
                String telepString = rs2.getString("phone_number");
                telephoneNums.add(telepString);
                System.out.println("happy");

            }
            System.out.println(vendorId +" "+ namev +" "+ street +" "+ area+"");
            return new Vendor(vendorId, namev, street, area, telephoneNums);
            
      }
        return null;
    }

    
    public Vendor getVendorByName(String name) throws SQLException {

        String selectSQL = "SELECT * FROM vendor WHERE vendor_name=?";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        preparedStatement.setString(1, name);
        ResultSet rs = preparedStatement.executeQuery();
        if (rs.first()) {
            System.out.println("hai");
            int vendorId = rs.getInt("vendor_id");
            String namev = rs.getString("vendor_name");
            String street = rs.getString("street");
            String area = rs.getString("area");
            ArrayList<String> telephoneNums = new ArrayList<String>();
            String selectSQL2 = "SELECT * FROM vendor_contact WHERE vendor_id =?";

            PreparedStatement preparedStatement2 = dbConnection.prepareStatement(selectSQL2);
            preparedStatement2.setInt(1, vendorId);
            ResultSet rs2 = preparedStatement2.executeQuery();
            
            while (rs2.next()) {
                String telepString = rs2.getString("phone_number");
                telephoneNums.add(telepString);
                System.out.println("happy");

            }
            System.out.println(vendorId +" "+ namev +" "+ street +" "+ area+"");
            return new Vendor(vendorId, namev, street, area, telephoneNums);
            
      }
        return null;
    }

    public int getLastVendorId() throws SQLException {

        String selectSQL = "SELECT vendor_id FROM vendor  ORDER BY vendor_id DESC LIMIT 1";
        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
        ResultSet rs = preparedStatement.executeQuery(selectSQL);

        if (rs.next()) {
            return rs.getInt("vendor_id");
        }
        return -1;
    }

    public List<Vendor> getVendorList() {
        ArrayList<Vendor> vendors = new ArrayList<Vendor>();

        try {

            String selectSQL = "SELECT * FROM vendor";

            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                ArrayList<String> telephoneNums = new ArrayList<String>();
                int vendorId = rs.getInt("vendor_id");
                String name = rs.getString("vendor_name");
                String street = rs.getString("street");
                String area = rs.getString("area");
                String selectSQL2 = "SELECT * FROM vendor_contact WHERE vendor_id =?";

                PreparedStatement preparedStatement2 = dbConnection.prepareStatement(selectSQL2);
                preparedStatement2.setInt(1, vendorId);
                ResultSet rs2 = preparedStatement2.executeQuery();
                while (rs2.next()) {
                    String telepString = rs2.getString("phone_number");
                    telephoneNums.add(telepString);

                }
                vendors.add(new Vendor(vendorId, name, street, area, telephoneNums));
                telephoneNums = new ArrayList<>();
            }

        } catch (SQLException ex) {
            Logger.getLogger(LocationDA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vendors;
    }
//    public void updateVendor(Vendor vendor,List tele){
//        try {
//            String updateSQL = "UPDATE vendor SET vendor_name =?, street=?,area = ? WHERE vendor_id =?  ";
//            PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL);
//            preparedStatement.setString(1, vendor.getVendorName());
//            preparedStatement.setString(2, vendor.getStreet());
//            preparedStatement.setString(3, vendor.getArea());
//            preparedStatement.setInt(4, vendor.getVendorId());
//            preparedStatement.executeUpdate();
//            updateVendorcontact(vendor,tele);
//        } catch (SQLException ex) {
//            Logger.getLogger(vendorDA.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//    }
//    public void updateVendorcontact(Vendor vendor,List tele){
//        String updateSQL1 = "UPDATE vendor_contact SET  phone_number=?  WHERE vendor_id=? AND phone_number = ?";
//        System.out.println(vendor.getTelephoneNum().size());
//        if (vendor.getTelephoneNum().size() > 0) {
//            for (int i = 0; i < vendor.getTelephoneNum().size(); i++) {
//                try {
//                    PreparedStatement preparedStatement = dbConnection.prepareStatement(updateSQL1);
//                    
//                    preparedStatement.setInt(2, this.getLastVendorId());
//                    preparedStatement.setString(1, vendor.getTelephoneNum().get(i).toString());
//                    if(i==0){
//                        preparedStatement.setString(3,tele.get(0)+"" );
//                        System.out.println("helloooo");
//                    }
//                    if(i==1){
//                       preparedStatement.setString(3,tele.get(1)+"" ); 
//                        System.out.println("haaii");
//                    }
//                    preparedStatement.executeUpdate();
//                } catch (SQLException ex) {
//                    Logger.getLogger(vendorDA.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
//
//    
//    public Vendor getVendorByID(int id) throws SQLException {
//
//        String selectSQL = "SELECT * FROM vendor WHERE vendor_id=?";
//        PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
//        preparedStatement.setInt(1, id);
//        ResultSet rs = preparedStatement.executeQuery();
//        if (rs.first()) {
//            System.out.println("hai");
//            int vendorId = rs.getInt("vendor_id");
//            String namev = rs.getString("vendor_name");
//            String street = rs.getString("street");
//            String area = rs.getString("area");
//            ArrayList<String> telephoneNums = new ArrayList<String>();
//            String selectSQL2 = "SELECT * FROM vendor_contact WHERE vendor_id =?";
//
//            PreparedStatement preparedStatement2 = dbConnection.prepareStatement(selectSQL2);
//            preparedStatement2.setInt(1, vendorId);
//            ResultSet rs2 = preparedStatement2.executeQuery();
//            
//            while (rs2.next()) {
//                String telepString = rs2.getString("phone_number");
//                telephoneNums.add(telepString);
//                System.out.println("happy");
//
//            }
//            System.out.println(vendorId +" "+ namev +" "+ street +" "+ area+"");
//            return new Vendor(vendorId, namev, street, area, telephoneNums);
//            
//      }
//        return null;
//    }
}
