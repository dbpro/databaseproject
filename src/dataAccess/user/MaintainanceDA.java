/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess.user;

import businessLogic.user.Maintains;
import dataAccess.DatabaseConnectionHandler;
import dataAccess.recordDA;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chath
 */
public class MaintainanceDA {

    private Connection dbConnection;

    public MaintainanceDA() {
        dbConnection = DatabaseConnectionHandler.createConnection();
    }

    public ArrayList<Maintains> getMaintainsRecords() {
        try {
            ArrayList<Maintains> list = new ArrayList<>();
            String selectSQL = "SELECT * FROM maintains";
            PreparedStatement preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Maintains record = new Maintains();
                record.setUsrId(rs.getInt("usr_id"));
                record.setSensorId(rs.getInt("sensor_id"));
                record.setRepDate(rs.getDate("rep_date"));
                record.setRepTime(rs.getTime("rep_time"));
                record.setDescription(rs.getString("description"));
                list.add(record);
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void addMaintainsRecord(Maintains m) {
        String query = "insert into maintains (usr_id, sensor_id, rep_date, rep_time, description) values (?, ?, ?,?, ?)";
        try {
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setInt(1, m.getUsrId());
            preparedStmt.setInt(2, m.getSensorId());
            preparedStmt.setDate(3, new java.sql.Date(m.getRepDate().getTime()));
            preparedStmt.setTime(4, m.getRepTime());
            preparedStmt.setString(5, m.getDescription());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String checkSensor(int sensorId) {
        String selectSQL = "select sensor_type FROM sensor where sensor_id=?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, sensorId);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return rs.getString("sensor_type");
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(MaintainanceDA.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
