/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "recordtb")
@NamedQueries({
    @NamedQuery(name = "Recordtb.findAll", query = "SELECT r FROM Recordtb r")})
public class Recordtb implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    @Basic(optional = false)
    @Column(name = "record_date")
    @Temporal(TemporalType.DATE)
    private Date recordDate;
    @Basic(optional = false)
    @Column(name = "record_time")
    @Temporal(TemporalType.TIME)
    private Date recordTime;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "recordtb")
    private WaterRecord waterRecord;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "recordtb")
    private Illuminance illuminance;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "recordtb")
    private AirRecord airRecord;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "recordtb")
    private RainfallRecord rainfallRecord;
    @JoinColumn(name = "location_id", referencedColumnName = "location_id")
    @ManyToOne
    private Location locationId;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "recordtb")
    private WaterLvl waterLvl;

    public Recordtb() {
    }

    public Recordtb(Integer recordId) {
        this.recordId = recordId;
    }

    public Recordtb(Integer recordId, Date recordDate, Date recordTime) {
        this.recordId = recordId;
        this.recordDate = recordDate;
        this.recordTime = recordTime;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public Date getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Date recordTime) {
        this.recordTime = recordTime;
    }

    public WaterRecord getWaterRecord() {
        return waterRecord;
    }

    public void setWaterRecord(WaterRecord waterRecord) {
        this.waterRecord = waterRecord;
    }

    public Illuminance getIlluminance() {
        return illuminance;
    }

    public void setIlluminance(Illuminance illuminance) {
        this.illuminance = illuminance;
    }

    public AirRecord getAirRecord() {
        return airRecord;
    }

    public void setAirRecord(AirRecord airRecord) {
        this.airRecord = airRecord;
    }

    public RainfallRecord getRainfallRecord() {
        return rainfallRecord;
    }

    public void setRainfallRecord(RainfallRecord rainfallRecord) {
        this.rainfallRecord = rainfallRecord;
    }

    public Location getLocationId() {
        return locationId;
    }

    public void setLocationId(Location locationId) {
        this.locationId = locationId;
    }

    public WaterLvl getWaterLvl() {
        return waterLvl;
    }

    public void setWaterLvl(WaterLvl waterLvl) {
        this.waterLvl = waterLvl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recordtb)) {
            return false;
        }
        Recordtb other = (Recordtb) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Recordtb[ recordId=" + recordId + " ]";
    }
    
}
