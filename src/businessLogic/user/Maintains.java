/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import businessLogic.Maintainance;
import businessLogic.Sensor;
import java.sql.Time;
import java.util.Date;

/**
 *
 * @author chath
 */
public class Maintains{

    private static final long serialVersionUID = 1L;

    private int usrId;
    private int sensorId;
    private Date repDate;
    private Time repTime;
    private String description;
    public Maintains() {
    }

    public Maintains(int usrId, int sensorId, Date repDate, Time repTime, String desc) {
        this.usrId = usrId;
        this.sensorId = sensorId;
        this.repDate = repDate;
        this.repTime = repTime;
        this.description=desc;
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public Date getRepDate() {
        return repDate;
    }

    public void setRepDate(Date repDate) {
        this.repDate = repDate;
    }

    public Time getRepTime() {
        return repTime;
    }

    public void setRepTime(Time repTime) {
        this.repTime = repTime;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
