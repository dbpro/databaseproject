/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "vendor")
@NamedQueries({
    @NamedQuery(name = "Vendor.findAll", query = "SELECT v FROM Vendor v")})
public class Vendor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "vendor_id")
    private Integer vendorId;
    @Basic(optional = false)
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "street")
    private String street;
    @Column(name = "area")
    private String area;
    @OneToMany(mappedBy = "vendorId")
    private Collection<Sensor> sensorCollection;
     private List<String> telephoneNum;

    public Vendor() {
    }

    public Vendor(Integer vendorId) {
        this.vendorId = vendorId;
    }

    public Vendor(Integer vendorId, String vendorName) {
        this.vendorId = vendorId;
        this.vendorName = vendorName;
    }
     public Vendor(Integer vendorId, String vendorName,String street,String area,List<String> telephoneNum) {
        this.vendorId = vendorId;
        this.vendorName = vendorName;
        this.street = street;
        this.area = area;
        this.telephoneNum = telephoneNum;
    }

    public Integer getVendorId() {
        return vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }
    public void setTelephoneNum(List<String> telephonenum){
        this.telephoneNum = telephonenum;
    }
    public List getTelephoneNum(){
        return telephoneNum;
    
}

    public String getVendorName() {
        return vendorName;
    }
    
    

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Collection<Sensor> getSensorCollection() {
        return sensorCollection;
    }

    public void setSensorCollection(Collection<Sensor> sensorCollection) {
        this.sensorCollection = sensorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vendorId != null ? vendorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendor)) {
            return false;
        }
        Vendor other = (Vendor) object;
        if ((this.vendorId == null && other.vendorId != null) || (this.vendorId != null && !this.vendorId.equals(other.vendorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Vendor[ vendorId=" + vendorId + " ]";
    }
    
}
