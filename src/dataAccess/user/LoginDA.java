/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccess.user;

import businessLogic.user.User;
import dataAccess.DatabaseConnectionHandler;
import dataAccess.recordDA;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ChathurangaKCD
 */
public class LoginDA {

    private final Connection dbConnection;

    public LoginDA() {
        this.dbConnection = DatabaseConnectionHandler.createConnection();
    }

    public void setLogin(User user) {
        try {
            String query = "update usertb set username=? , password=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getUsername());
            preparedStmt.setString(2, user.getPassword());
            preparedStmt.setInt(3, user.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void editUsername(User user) {
        try {
            String query = "update usertb set username=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getUsername());
            preparedStmt.setInt(2, user.getUsrId());
            System.out.println(user.getUsername() + "byfyfyvy");
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public void editPassword(User user) {
        try {
            String query = "update usertb set password=? where usr_id=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, user.getPassword());
            preparedStmt.setInt(2, user.getUsrId());
            preparedStmt.execute();
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public boolean isUsernameAvailable(String username) {
        try {
            String query = "select usr_id from usertb where username=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, username);
            ResultSet rs = preparedStmt.executeQuery();
            return (!rs.next());
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

    public int getUserBy(String NIC) {
        try {
            String query = "select usr_id from usertb where NIC=?";
            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
            preparedStmt.setString(1, NIC);
            ResultSet rs = preparedStmt.executeQuery();
            if(rs.next()){
                return rs.getInt("usr_id");
            }
            return 0;
        } catch (SQLException ex) {
            Logger.getLogger(recordDA.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        } finally {
//            DatabaseConnectionHandler.closeConnection();
        }
    }

//    public Object[] login(String username) {
//        try {
//            String query = "select usr_id,password from usertb where username=?";
//            PreparedStatement preparedStmt = dbConnection.prepareStatement(query);
//            preparedStmt.setString(1, username);
//            ResultSet rs = preparedStmt.executeQuery();
//            if (rs.next()) {
//                return new Object[]{rs.getInt("usr_id"),rs.getS};
//            }
//            return -1;
//        } catch (SQLException ex) {
//            return -2;
//        } finally {
////            DatabaseConnectionHandler.closeConnection();
//        }
//    }
}
