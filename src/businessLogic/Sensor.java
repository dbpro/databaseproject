/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "sensor")
@NamedQueries({
    @NamedQuery(name = "Sensor.findAll", query = "SELECT s FROM Sensor s")})
public class Sensor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sensor_id")
    private Integer sensorId;
    @Basic(optional = false)
    @Column(name = "sensor_type")
    private String sensorType;
    @Basic(optional = false)
    @Column(name = "sensor_status")
    private String sensorStatus;
    @Basic(optional = false)
    @Column(name = "date_installed")
    @Temporal(TemporalType.DATE)
    private Date dateInstalled;
    @Column(name = "last_check_date")
    @Temporal(TemporalType.DATE)
    private Date lastCheckDate;
    @Basic(optional = false)
    @Column(name = "expected_expiry_date")
    @Temporal(TemporalType.DATE)
    private Date expectedExpiryDate;
    @JoinColumn(name = "vendor_id", referencedColumnName = "vendor_id")
    @ManyToOne
    private int vendorId;
    @JoinColumn(name = "vendor_id", referencedColumnName = "vendor_id")
    @ManyToOne
    private int locationId;
    

    public Sensor() {
    }

    public Sensor(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public Sensor(Integer sensorId, String sensorType, String sensorStatus, Date dateInstalled, Date expectedExpiryDate) {
        this.sensorId = sensorId;
        this.sensorType = sensorType;
        this.sensorStatus = sensorStatus;
        this.dateInstalled = dateInstalled;
        this.expectedExpiryDate = expectedExpiryDate;
    }
     public Sensor(Integer sensorId, String sensorType, String sensorStatus, Date dateInstalled,Date lastCheckDate, Date expectedExpiryDate) {
        this.sensorId = sensorId;
        this.sensorType = sensorType;
        this.sensorStatus = sensorStatus;
        this.dateInstalled = dateInstalled;
        this.lastCheckDate = lastCheckDate;
        this.expectedExpiryDate = expectedExpiryDate;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getSensorStatus() {
        return sensorStatus;
    }

    public void setSensorStatus(String sensorStatus) {
        this.sensorStatus = sensorStatus;
    }

    public Date getDateInstalled() {
        return dateInstalled;
    }

    public void setDateInstalled(Date dateInstalled) {
        this.dateInstalled = dateInstalled;
    }

    public Date getLastCheckDate() {
        return lastCheckDate;
    }

    public void setLastCheckDate(Date lastCheckDate) {
        this.lastCheckDate = lastCheckDate;
    }

    public Date getExpectedExpiryDate() {
        return expectedExpiryDate;
    }

    public void setExpectedExpiryDate(Date expectedExpiryDate) {
        this.expectedExpiryDate = expectedExpiryDate;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sensorId != null ? sensorId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sensor)) {
            return false;
        }
        Sensor other = (Sensor) object;
        if ((this.sensorId == null && other.sensorId != null) || (this.sensorId != null && !this.sensorId.equals(other.sensorId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Sensor[ sensorId=" + sensorId + " ]";
    }

    /**
     * @return the locationId
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * @param locationId the locationId to set
     */
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    /**
     * @return the vendorId
     */
    public int getVendorId() {
        return vendorId;
    }

    /**
     * @param vendorId the vendorId to set
     */
    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }
    
}
