/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewLayer.sensor;

import businessLogic.Location;
import businessLogic.Sensor;
import businessLogic.Vendor;
import businessLogic.user.UserHandler;
import dataAccess.LocationDA;
import dataAccess.sensorDA;
import dataAccess.vendorDA;
import java.awt.CardLayout;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author HP
 */
public class SensorUI extends javax.swing.JFrame {

    /**
     * Creates new form SensorUI
     */
    sensorDA sensorda = new sensorDA();
    LocationDA locationDA = new LocationDA();
    vendorDA vendorda = new vendorDA();
    UserHandler userhandler = UserHandler.getInstance();

    DefaultTableModel sensorDetailsTableModel;
    DefaultTableModel vendorDetailsTableModel;
    DefaultComboBoxModel addsensorTypeComboBoxModel;
    DefaultComboBoxModel addvendorComboBoxModel;
    DefaultComboBoxModel addlocComboBoxModel;
    DefaultComboBoxModel addvendorComboBoxModel1;
    DefaultComboBoxModel addlocComboBoxModel1;
    public SensorUI() {
        initComponents();
        sensorDetailsTableModel = (DefaultTableModel) SensorDetailsTbl.getModel();
        addsensorTypeComboBoxModel = (DefaultComboBoxModel)searchsensoridjComboBox1.getModel();
        addvendorComboBoxModel1 = (DefaultComboBoxModel)addsensorvendorjComboBox2.getModel();
        addlocComboBoxModel1 = (DefaultComboBoxModel)addsensorlocjComboBox3.getModel();
        addlocComboBoxModel = (DefaultComboBoxModel)jComboBox1Searchloc.getModel();
        addvendorComboBoxModel = (DefaultComboBoxModel)jComboBox1searchvendor.getModel();
        loadDetails();
        getLastSensorId();
    }
    public void loadDetails(){
         
        addvendorComboBoxModel.removeAllElements();
        addsensorTypeComboBoxModel.removeAllElements();
        addlocComboBoxModel.removeAllElements();
        addlocComboBoxModel1.removeAllElements();
        addvendorComboBoxModel1.removeAllElements();
        List<Vendor> vendors = vendorda.getVendorList();
        List<Sensor> sensors = sensorda.getsensorList();
        List<Location> locations = locationDA.getLocationList();
        for (Vendor vendor1 : vendors) {
            addvendorComboBoxModel.addElement(vendor1.getVendorName());
            addvendorComboBoxModel1.addElement(vendor1.getVendorName());
        }
         addvendorComboBoxModel.setSelectedItem(null);
         addvendorComboBoxModel1.setSelectedItem(null);
          for (Sensor sensor1 : sensors) {
            addsensorTypeComboBoxModel.addElement(sensor1.getSensorId());
        }
         addsensorTypeComboBoxModel.setSelectedItem(null);
      for (Location loc1 : locations) {
            addlocComboBoxModel.addElement(loc1.getLocationName());
            addlocComboBoxModel1.addElement(loc1.getLocationName());
        }
         addlocComboBoxModel.setSelectedItem(null);
         addlocComboBoxModel1.setSelectedItem(null);
    

    }
    public void getLastSensorId() {
        try {
            int sensorID = sensorda.getLastSensorId();
            if (sensorID > 0) {
                sensorIDTxt.setText(sensorID + 1 + "");;
            } else {
                sensorIDTxt.setText(String.valueOf(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setTableSensors(List<Sensor> sensorsDetails) {
        sensorDetailsTableModel.setRowCount(0);
        for (int i = 0; i < sensorsDetails.size(); i++) {

            Sensor sensor = sensorsDetails.get(i);
            sensorDetailsTableModel.addRow(new Object[]{sensor.getSensorId().toString(), sensor.getSensorType(), sensor.getVendorId(), sensor.getLocationId(), sensor.getSensorStatus(), sensor.getDateInstalled(), sensor.getExpectedExpiryDate(), sensor.getLastCheckDate()});

        }
    }

    public void setTableVendors(List<Vendor> vendorsDetails) {
        ((DefaultTableModel) vendorDetailsTbl1.getModel()).setRowCount(0);
        String l = "";
        for (Vendor c : vendorsDetails) {
            int row = vendorDetailsTbl1.getRowCount();
            if (c.getTelephoneNum().isEmpty()) {
                l += "      --       ";
            } else {
                for (int j = 0; j < c.getTelephoneNum().size(); j++) {
                    l += c.getTelephoneNum().get(j);
                    if (j == 0 && c.getTelephoneNum().size() == 2) {
                        l += "  /   ";
                    }
                }
            }
            System.out.println("setting table vendors");
            ((DefaultTableModel) vendorDetailsTbl1.getModel()).addRow(new Object[]{c.getVendorId() + "", c.getVendorName(), c.getStreet(), c.getArea(), l});
            l = "";
        }
    }

    public boolean validateName(String txt) {

        String regx = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();

    }

    public boolean validateNumbers(String x) {
        try {
            int l = Integer.parseInt(x);
            return true;
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(this, "Enter a valid numbers");
            return false;
        }
    }

    public void addVendor(List telephoneNum) {
        try {

            Vendor vendor = new Vendor();
            vendor.setVendorId(vendorda.getLastVendorId());

            vendor.setStreet(streetVendorTxt1.getText());
            vendor.setArea(areavendorTxt1.getText());
            vendor.setVendorName(nameTxt2.getText());
            vendor.setTelephoneNum(telephoneNum);
            vendorda.insertVendor(vendor);
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void addVendorClear() {

        streetVendorTxt1.setText(null);
        areavendorTxt1.setText(null);
        nameTxt2.setText(null);
        addvendorTele1txt.setText(null);
        addvendortele2txt.setText(null);
    }

    public void addSensor() {
        Logger.getLogger(SensorUI.class.getName()).log(Level.INFO, null, "Insert a new sensor details");
        try {
            Sensor sensor = new Sensor();
            sensor.setSensorId(Integer.parseInt(sensorIDTxt.getText()));
            sensor.setExpectedExpiryDate(addSensorDate.getDate());
            sensor.setSensorType(sensortypeTxt1.getText());
            if (addsensorjRadioButton1.isSelected()) {
                sensor.setSensorStatus("R");
            }
            if (addsensorjRadioButton2.isSelected()) {
                sensor.setSensorStatus("E");
            }
                String loc4=addlocComboBoxModel1.getSelectedItem().toString();
                String ven4 = addvendorComboBoxModel1.getSelectedItem().toString();
                 Vendor vendor = new Vendor();
            vendor = vendorda.getVendorByName(ven4);
            
            Location loc  = new Location();
            loc = locationDA.getLocationByName(loc4);
           System.out.println("vendor"+ven4);
           
            sensor.setLocationId(loc.getLocationId());
            sensor.setVendorId(vendor.getVendorId());
            sensor.setLastCheckDate(null);
            sensor.setDateInstalled(null);
            sensorda.insertSensor(sensor);
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete() {
        int row = SensorDetailsTbl.getSelectedRow();

        int id = Integer.parseInt(SensorDetailsTbl.getValueAt(row, 0).toString());

        System.out.println(id);
        String message = "Are you sure you want to delete?";
        String title = "Delete";
        // display the JOptionPane showConfirmDialog
        int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            this.sensorDetailsTableModel.setRowCount(0);
            sensorda.deleteSensor(id);
            //  this.setTableSensors(null);
            this.setTableSensors(sensorda.getsensorList());
        } else {
            return;
        }
        // this.SensorDetailsTbl.removeRowSelectionInterval(row, row+1);

        //this.setTableSensors(sensorda.getsensorList());
    }

    public void clearAll() {
//        searchvendorTxt3.setText(null);
//        searchSensorTypeTxt4.setText(null);
//        searchLocationTxt5.setText(null);
        loadDetails();
        searchResultvendorrIdLbl7.setText(null);
        searchResultvendorrNameLbl7.setText(null);
        searchResultvendorrStreetLbl7.setText(null);
        searchResultvendorrAreaLbl7.setText(null);
        searchResultvendorrtele1Lbl7.setText(null);
        searchResultvendortele2Lbl7.setText(null);
        searchResultSensorrIdLbl8.setText(null);
        searchResultSensorTypeTxt.setText(null);
        searchResultSensorStatusTxt.setText(null);
        searchResultDateInstalledTxt.setText(null);
        searchResultExpectedExpiryDateTxt.setText(null);
        searchResultLastCheckedDateTxt.setText(null);
        searchResultLocationIdLbl9.setText(null);
        searchResultLocationrNameLbl9.setText(null);
        searchResultLocationAreaLbl9.setText(null);
        searchResultLocationrStreetLbl9.setText(null);
        searchResultLocationLatitudeLbl9.setText(null);
        searchResultLocationLogitudeLbl9.setText(null);

    }

    public void searchVendor(String name) {
        try {
            Vendor vendor = new Vendor();
            vendor = vendorda.getVendorByName(name);

            if (vendor != null) {
                searchResultvendorrIdLbl7.setText(vendor.getVendorId() + "");
                searchResultvendorrNameLbl7.setText(vendor.getVendorName());
                searchResultvendorrStreetLbl7.setText(vendor.getStreet());
                searchResultvendorrAreaLbl7.setText(vendor.getArea());
                for (int i = 0; i < vendor.getTelephoneNum().size(); i++) {

                    searchResultvendorrtele1Lbl7.setText(vendor.getTelephoneNum().get(i).toString());
                    searchResultvendortele2Lbl7.setText(vendor.getTelephoneNum().get(i).toString());
                }
            } else {
                JOptionPane.showMessageDialog(sensorDetails, "Enter a valid vendor name");
                return;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchSensorType(String id) {
        try {
            Sensor sensor = new Sensor();

            sensor = sensorda.getSensorDetails1(Integer.parseInt(id));
            System.out.println(sensor.getSensorId());
            if (sensor != null) {
                System.out.println(sensor.getSensorId());
                searchResultSensorrIdLbl8.setText(sensor.getSensorId() + "");
                searchResultSensorTypeTxt.setText(sensor.getSensorType() + "");
                searchResultSensorStatusTxt.setText(sensor.getSensorStatus() + "");
                searchResultDateInstalledTxt.setText(sensor.getDateInstalled() + "");
                searchResultExpectedExpiryDateTxt.setText(sensor.getExpectedExpiryDate() + "");
                searchResultLastCheckedDateTxt.setText(sensor.getLastCheckDate() + "");
            } else {
                JOptionPane.showMessageDialog(sensorDetails, "Enter a valid sensor type");
                return;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void searchLocation(String name) {
        try {
            Location location1 = new Location();
            location1 = locationDA.getLocationByName(name);
            if (location1 != null) {
                searchResultLocationIdLbl9.setText(location1.getLocationId() + "");
                searchResultLocationrNameLbl9.setText(name);
                searchResultLocationAreaLbl9.setText(location1.getArea() + "");
                searchResultLocationrStreetLbl9.setText(location1.getStreet() + "");
                searchResultLocationLatitudeLbl9.setText(location1.getLatitude() + "");
                searchResultLocationLogitudeLbl9.setText(location1.getLongitude() + "");

            } else {
                JOptionPane.showMessageDialog(sensorDetails, "Enter a valid location name");
                return;
            }

        } catch (Exception ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jScrollPane1 = new javax.swing.JScrollPane();
        jXTable1 = new org.jdesktop.swingx.JXTable();
        jDialog1 = new javax.swing.JDialog();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        SensorTaskContainer = new org.jdesktop.swingx.JXTaskPaneContainer();
        Search = new org.jdesktop.swingx.JXTaskPane();
        sensorSearchBtn = new org.jdesktop.swingx.JXButton();
        sensorManage = new org.jdesktop.swingx.JXTaskPane();
        sensorDetailsBtn = new org.jdesktop.swingx.JXButton();
        sensorAddBtn = new org.jdesktop.swingx.JXButton();
        vendorManage = new org.jdesktop.swingx.JXTaskPane();
        vendorDetailsBtn2 = new org.jdesktop.swingx.JXButton();
        addVendorbtn = new org.jdesktop.swingx.JXButton();
        sensorDetails = new javax.swing.JPanel();
        cardPanel = new javax.swing.JPanel();
        addSensorPanel = new javax.swing.JPanel();
        addSensor = new javax.swing.JPanel();
        sensorIdLbl = new javax.swing.JLabel();
        sensorTypeLbl = new javax.swing.JLabel();
        sensorExpiryLbl = new javax.swing.JLabel();
        addSensorvendorLbl = new javax.swing.JLabel();
        saveBtn = new javax.swing.JButton();
        clearBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        SensorAddTopic = new javax.swing.JLabel();
        sensorIDTxt = new javax.swing.JTextField();
        addSensorDate = new org.jdesktop.swingx.JXDatePicker();
        sensorIdLbl1 = new javax.swing.JLabel();
        sensorIdLbl3 = new javax.swing.JLabel();
        addsensorjRadioButton1 = new javax.swing.JRadioButton();
        addsensorjRadioButton2 = new javax.swing.JRadioButton();
        addsensorvendorjComboBox2 = new javax.swing.JComboBox();
        addsensorlocjComboBox3 = new javax.swing.JComboBox();
        sensortypeTxt1 = new javax.swing.JTextField();
        addsensorjRadioButton1 = new javax.swing.JRadioButton();
        addsensorjRadioButton2 = new javax.swing.JRadioButton();
        manageSensorInfo = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        SensorDetailsTbl = new javax.swing.JTable();
        SensorAddTopic2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        deleteBtn = new javax.swing.JButton();
        editBtn = new javax.swing.JButton();
        addVendorPanel1 = new javax.swing.JPanel();
        addSensor1 = new javax.swing.JPanel();
        sensorIdLbl2 = new javax.swing.JLabel();
        sensorTypeLbl1 = new javax.swing.JLabel();
        addSensorvendorLbl1 = new javax.swing.JLabel();
        saveBtn1 = new javax.swing.JButton();
        clearBtn1 = new javax.swing.JButton();
        cancelBtn1 = new javax.swing.JButton();
        SensorAddTopic1 = new javax.swing.JLabel();
        vendorIDTxt1 = new javax.swing.JTextField();
        streetVendorTxt1 = new javax.swing.JTextField();
        sensorIdLbl4 = new javax.swing.JLabel();
        areavendorTxt1 = new javax.swing.JTextField();
        sensorIdLbl5 = new javax.swing.JLabel();
        nameTxt2 = new javax.swing.JTextField();
        addvendortele2txt = new javax.swing.JTextField();
        addvendorTele1txt = new javax.swing.JTextField();
        manageVendorInfo1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        vendorDetailsTbl1 = new javax.swing.JTable();
        SensorAddTopic3 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        editBtn1 = new javax.swing.JButton();
        deleteBtn2 = new javax.swing.JButton();
        editVendorPanel2 = new javax.swing.JPanel();
        editVendor2 = new javax.swing.JPanel();
        editVendorIdLbl6 = new javax.swing.JLabel();
        editvendorTypeLbl2 = new javax.swing.JLabel();
        editVendorvendorLbl2 = new javax.swing.JLabel();
        editVendorTopic5 = new javax.swing.JLabel();
        editVendorIDTxt2 = new javax.swing.JTextField();
        editVendorstreetTxt2 = new javax.swing.JTextField();
        editVendorarearTxt2 = new javax.swing.JTextField();
        editVendorIdLbl7 = new javax.swing.JLabel();
        editVendorIdLbl8 = new javax.swing.JLabel();
        editVendornameTxt3 = new javax.swing.JTextField();
        editVendortele1txt = new javax.swing.JTextField();
        editVendortele1txt1 = new javax.swing.JTextField();
        editVendorsaveBtn2 = new javax.swing.JButton();
        searchPanel2 = new javax.swing.JPanel();
        addSensor2 = new javax.swing.JPanel();
        SensorAddTopic4 = new javax.swing.JLabel();
        SearchByjPanel1 = new javax.swing.JPanel();
        searchByLbl = new javax.swing.JLabel();
        jXPanel1 = new org.jdesktop.swingx.JXPanel();
        vendorLbl2 = new javax.swing.JRadioButton();
        vendorLbl1 = new javax.swing.JRadioButton();
        vendorLbl = new javax.swing.JRadioButton();
        SearchResultjPanel2 = new javax.swing.JPanel();
        addSensorvendorLbl2 = new javax.swing.JLabel();
        searchResultCardPanel = new org.jdesktop.swingx.JXPanel();
        vendorSearchResult = new javax.swing.JPanel();
        searchvendorrIdLbl7 = new javax.swing.JLabel();
        searchvendorStreetLbl7 = new javax.swing.JLabel();
        searchvendorrNameLbl7 = new javax.swing.JLabel();
        searchvendorrTeleLbl7 = new javax.swing.JLabel();
        searchvendorrAreaLbl7 = new javax.swing.JLabel();
        searchResultvendorrStreetLbl7 = new javax.swing.JLabel();
        searchResultvendorrNameLbl7 = new javax.swing.JLabel();
        searchResultvendorrtele1Lbl7 = new javax.swing.JLabel();
        searchResultvendorrAreaLbl7 = new javax.swing.JLabel();
        searchResultvendorrIdLbl7 = new javax.swing.JLabel();
        searchResultvendortele2Lbl7 = new javax.swing.JLabel();
        sensorTypeSearchResult = new javax.swing.JPanel();
        searchvendorrIdLbl8 = new javax.swing.JLabel();
        searchvendorStreetLbl8 = new javax.swing.JLabel();
        searchvendorrNameLbl8 = new javax.swing.JLabel();
        searchvendorrTeleLbl8 = new javax.swing.JLabel();
        searchvendorrAreaLbl8 = new javax.swing.JLabel();
        searchResultDateInstalledTxt = new javax.swing.JLabel();
        searchResultSensorTypeTxt = new javax.swing.JLabel();
        searchResultExpectedExpiryDateTxt = new javax.swing.JLabel();
        searchResultLastCheckedDateTxt = new javax.swing.JLabel();
        searchResultSensorrIdLbl8 = new javax.swing.JLabel();
        searchvendorrNameLbl10 = new javax.swing.JLabel();
        searchResultSensorStatusTxt = new javax.swing.JLabel();
        locationSearchResult1 = new javax.swing.JPanel();
        searchvendorrIdLbl9 = new javax.swing.JLabel();
        searchvendorStreetLbl9 = new javax.swing.JLabel();
        searchvendorrNameLbl9 = new javax.swing.JLabel();
        searchvendorrTeleLbl9 = new javax.swing.JLabel();
        searchvendorrAreaLbl9 = new javax.swing.JLabel();
        searchResultLocationrStreetLbl9 = new javax.swing.JLabel();
        searchResultLocationrNameLbl9 = new javax.swing.JLabel();
        searchResultLocationLatitudeLbl9 = new javax.swing.JLabel();
        searchResultLocationAreaLbl9 = new javax.swing.JLabel();
        searchResultLocationIdLbl9 = new javax.swing.JLabel();
        searchResultLocationLogitudeLbl9 = new javax.swing.JLabel();
        searchvendorrTeleLbl10 = new javax.swing.JLabel();
        SearchjPanel6 = new javax.swing.JPanel();
        addSensorvendorLbl3 = new javax.swing.JLabel();
        searchCardPanel = new org.jdesktop.swingx.JXPanel();
        vendorSearchPanel = new javax.swing.JPanel();
        vendorIdLbl9 = new javax.swing.JLabel();
        searchBtnVendor = new javax.swing.JButton();
        jComboBox1searchvendor = new javax.swing.JComboBox();
        sensorTypeSearchPanel1 = new javax.swing.JPanel();
        sensoTypeLbl10 = new javax.swing.JLabel();
        searchSensorBtn = new javax.swing.JButton();
        searchsensoridjComboBox1 = new javax.swing.JComboBox();
        locationSearchPanel1 = new javax.swing.JPanel();
        LocationIdLbl11 = new javax.swing.JLabel();
        searchBtnLocation = new javax.swing.JButton();
        jComboBox1Searchloc = new javax.swing.JComboBox();
        editSensorPanel1 = new javax.swing.JPanel();
        editSensor3 = new javax.swing.JPanel();
        sensorIdLbl6 = new javax.swing.JLabel();
        sensorTypeLbl2 = new javax.swing.JLabel();
        sensorExpiryLbl1 = new javax.swing.JLabel();
        addSensorvendorLbl4 = new javax.swing.JLabel();
        saveBtn2 = new javax.swing.JButton();
        SensorAddTopic5 = new javax.swing.JLabel();
        sensorIDTxt1 = new javax.swing.JTextField();
        addSensorVendorTxt1 = new javax.swing.JTextField();
        addSensorDate1 = new org.jdesktop.swingx.JXDatePicker();
        sensorIdLbl7 = new javax.swing.JLabel();
        addSensorLocTxt1 = new javax.swing.JTextField();
        sensorIdLbl8 = new javax.swing.JLabel();
        sensortypeTxt2 = new javax.swing.JTextField();
        editSensorRadioButton1 = new javax.swing.JRadioButton();
        EditSensorRadioButton2 = new javax.swing.JRadioButton();

        jXTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jXTable1);

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(158, 158, 158));
        setBounds(new java.awt.Rectangle(0, 0, 600, 600));

        SensorTaskContainer.setBackground(new java.awt.Color(158, 158, 158));
        org.jdesktop.swingx.VerticalLayout verticalLayout1 = new org.jdesktop.swingx.VerticalLayout();
        verticalLayout1.setGap(2);
        SensorTaskContainer.setLayout(verticalLayout1);

        Search.setBackground(new java.awt.Color(158, 158, 158));
        Search.setForeground(new java.awt.Color(0, 0, 0));
        Search.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        Search.setLayout(new java.awt.GridBagLayout());

        sensorSearchBtn.setText(" Search");
        sensorSearchBtn.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorSearchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorSearchBtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 11, 13, 11);
        Search.add(sensorSearchBtn, gridBagConstraints);

        SensorTaskContainer.add(Search);

        sensorManage.setBackground(new java.awt.Color(158, 158, 158));
        sensorManage.setForeground(new java.awt.Color(51, 51, 51));
        sensorManage.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        sensorManage.setOpaque(false);
        sensorManage.setLayout(new java.awt.GridBagLayout());

        sensorDetailsBtn.setText("Sensor Details");
        sensorDetailsBtn.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorDetailsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorDetailsBtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 11, 0, 11);
        sensorManage.add(sensorDetailsBtn, gridBagConstraints);

        sensorAddBtn.setText("Add Sensor");
        sensorAddBtn.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorAddBtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 28;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 11, 13, 11);
        sensorManage.add(sensorAddBtn, gridBagConstraints);

        SensorTaskContainer.add(sensorManage);

        vendorManage.setBackground(new java.awt.Color(158, 158, 158));
        vendorManage.setForeground(new java.awt.Color(51, 51, 51));
        vendorManage.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        vendorManage.setOpaque(false);
        vendorManage.setTitle("Manage Vendors");
        vendorManage.setLayout(new java.awt.GridBagLayout());

        vendorDetailsBtn2.setText("Vendor Details");
        vendorDetailsBtn2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        vendorDetailsBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vendorDetailsBtn2ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 11, 0, 11);
        vendorManage.add(vendorDetailsBtn2, gridBagConstraints);

        addVendorbtn.setText("Add Vendor");
        addVendorbtn.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addVendorbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addVendorbtnActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 24;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 11, 13, 11);
        vendorManage.add(addVendorbtn, gridBagConstraints);

        SensorTaskContainer.add(vendorManage);

        sensorDetails.setBackground(new java.awt.Color(204, 204, 204));
        sensorDetails.setPreferredSize(new java.awt.Dimension(982, 509));

        cardPanel.setBackground(new java.awt.Color(204, 204, 204));
        cardPanel.setPreferredSize(new java.awt.Dimension(982, 509));
        cardPanel.setLayout(new java.awt.CardLayout());

        addSensorPanel.setBackground(new java.awt.Color(158, 158, 158));
        addSensorPanel.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        addSensorPanel.setPreferredSize(new java.awt.Dimension(982, 509));

        addSensor.setBackground(new java.awt.Color(204, 204, 204));
        addSensor.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        sensorIdLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl.setText("Sensor ID");

        sensorTypeLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorTypeLbl.setText("Sensor Type");

        sensorExpiryLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorExpiryLbl.setText("Expected Expiry Date");

        addSensorvendorLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addSensorvendorLbl.setText("Vendor");

        saveBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        saveBtn.setText("Save");
        saveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtnActionPerformed(evt);
            }
        });

        clearBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        clearBtn.setText("Clear");
        clearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBtnActionPerformed(evt);
            }
        });

        cancelBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        cancelBtn.setText("Cancel");
        cancelBtn.setPreferredSize(new java.awt.Dimension(70, 30));
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        SensorAddTopic.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic.setText("Add Sensor");
        SensorAddTopic.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic.setOpaque(true);

        sensorIDTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorIDTxtActionPerformed(evt);
            }
        });

        addSensorDate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSensorDateActionPerformed(evt);
            }
        });

        sensorIdLbl1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl1.setText("Location");

        sensorIdLbl3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl3.setText("Sensor Status");

        buttonGroup1.add(addsensorjRadioButton1);
        addsensorjRadioButton1.setText("Error");

        buttonGroup1.add(addsensorjRadioButton2);
        addsensorjRadioButton2.setText("Running");

        addsensorvendorjComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        addsensorvendorjComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addsensorvendorjComboBox2ActionPerformed(evt);
            }
        });

        addsensorlocjComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        addsensorlocjComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addsensorlocjComboBox3ActionPerformed(evt);
            }
        });

        sensortypeTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensortypeTxt1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(addsensorjRadioButton1);
        addsensorjRadioButton1.setText("Error");

        buttonGroup1.add(addsensorjRadioButton2);
        addsensorjRadioButton2.setText("Running");

        buttonGroup1.add(addsensorjRadioButton1);
        addsensorjRadioButton1.setText("Error");

        buttonGroup1.add(addsensorjRadioButton2);
        addsensorjRadioButton2.setText("Running");

        javax.swing.GroupLayout addSensorLayout = new javax.swing.GroupLayout(addSensor);
        addSensor.setLayout(addSensorLayout);
        addSensorLayout.setHorizontalGroup(
            addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensorLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(addSensorLayout.createSequentialGroup()
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SensorAddTopic, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addSensorLayout.createSequentialGroup()
                                .addComponent(sensorExpiryLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, addSensorLayout.createSequentialGroup()
                        .addComponent(sensorIdLbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, addSensorLayout.createSequentialGroup()
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sensorIdLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addSensorvendorLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorTypeLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorIdLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(addSensorLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(addSensorLayout.createSequentialGroup()
                                        .addComponent(addsensorjRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(addsensorjRadioButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(addSensorDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(addsensorlocjComboBox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18))
                            .addGroup(addSensorLayout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(addSensorLayout.createSequentialGroup()
                                        .addComponent(saveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(clearBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(cancelBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(sensorIDTxt, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18))
                            .addGroup(addSensorLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(addsensorvendorjComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18))
                            .addGroup(addSensorLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(sensortypeTxt1)
                                .addContainerGap())))))
        );
        addSensorLayout.setVerticalGroup(
            addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensorLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SensorAddTopic)
                .addGap(18, 18, 18)
                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIDTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorIdLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sensorTypeLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensortypeTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addSensorvendorLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addsensorvendorjComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addSensorLayout.createSequentialGroup()
                        .addComponent(sensorIdLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sensorIdLbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addsensorjRadioButton2)
                            .addComponent(addsensorjRadioButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addSensorDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorExpiryLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addGroup(addSensorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cancelBtn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(clearBtn)
                            .addComponent(saveBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(23, Short.MAX_VALUE))
                    .addGroup(addSensorLayout.createSequentialGroup()
                        .addComponent(addsensorlocjComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout addSensorPanelLayout = new javax.swing.GroupLayout(addSensorPanel);
        addSensorPanel.setLayout(addSensorPanelLayout);
        addSensorPanelLayout.setHorizontalGroup(
            addSensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensorPanelLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(addSensor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(209, Short.MAX_VALUE))
        );
        addSensorPanelLayout.setVerticalGroup(
            addSensorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensorPanelLayout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(addSensor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );

        cardPanel.add(addSensorPanel, "addSensor");

        manageSensorInfo.setBackground(new java.awt.Color(204, 204, 204));
        manageSensorInfo.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        manageSensorInfo.setPreferredSize(new java.awt.Dimension(982, 509));

        SensorDetailsTbl.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        SensorDetailsTbl.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        SensorDetailsTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Sensor ID", "Sensor Type", "Vendor ID", "Location ID", "Status", "Installed Date", "Date of Expiry", "Last Checked Datel"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        SensorDetailsTbl.setBackground(new java.awt.Color(204, 204, 204));
        SensorDetailsTbl.setSelectionBackground(new java.awt.Color(153, 153, 153));
        SensorDetailsTbl.setUpdateSelectionOnSort(false);
        jScrollPane2.setViewportView(SensorDetailsTbl);

        SensorAddTopic2.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic2.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic2.setText(" Sensor Details");
        SensorAddTopic2.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic2.setOpaque(true);

        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        deleteBtn.setBackground(new java.awt.Color(153, 153, 153));
        deleteBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        deleteBtn.setText("Delete");
        deleteBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtnActionPerformed(evt);
            }
        });

        editBtn.setBackground(new java.awt.Color(153, 153, 153));
        editBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        editBtn.setText("Edit");
        editBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(editBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(editBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(deleteBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout manageSensorInfoLayout = new javax.swing.GroupLayout(manageSensorInfo);
        manageSensorInfo.setLayout(manageSensorInfoLayout);
        manageSensorInfoLayout.setHorizontalGroup(
            manageSensorInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageSensorInfoLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(manageSensorInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SensorAddTopic2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(manageSensorInfoLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 844, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        manageSensorInfoLayout.setVerticalGroup(
            manageSensorInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageSensorInfoLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(SensorAddTopic2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(manageSensorInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
                    .addGroup(manageSensorInfoLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        cardPanel.add(manageSensorInfo, "manageSensorInfo");

        addVendorPanel1.setBackground(new java.awt.Color(158, 158, 158));
        addVendorPanel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        addVendorPanel1.setPreferredSize(new java.awt.Dimension(982, 509));

        addSensor1.setBackground(new java.awt.Color(204, 204, 204));
        addSensor1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        sensorIdLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl2.setText("Vendor ID");

        sensorTypeLbl1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorTypeLbl1.setText("Name");

        addSensorvendorLbl1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addSensorvendorLbl1.setText("Street");

        saveBtn1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        saveBtn1.setText("Save");
        saveBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtn1ActionPerformed(evt);
            }
        });

        clearBtn1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        clearBtn1.setText("Clear");
        clearBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBtn1ActionPerformed(evt);
            }
        });

        cancelBtn1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        cancelBtn1.setText("Cancel");
        cancelBtn1.setPreferredSize(new java.awt.Dimension(70, 30));
        cancelBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtn1ActionPerformed(evt);
            }
        });

        SensorAddTopic1.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic1.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic1.setText("Add Vendor");
        SensorAddTopic1.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic1.setOpaque(true);

        vendorIDTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vendorIDTxt1ActionPerformed(evt);
            }
        });

        sensorIdLbl4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl4.setText("Area");

        areavendorTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                areavendorTxt1ActionPerformed(evt);
            }
        });

        sensorIdLbl5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl5.setText("Telephone Number");

        nameTxt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameTxt2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout addSensor1Layout = new javax.swing.GroupLayout(addSensor1);
        addSensor1.setLayout(addSensor1Layout);
        addSensor1Layout.setHorizontalGroup(
            addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensor1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SensorAddTopic1, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(addSensor1Layout.createSequentialGroup()
                        .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sensorIdLbl4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addSensorvendorLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorTypeLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorIdLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorIdLbl5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(addSensor1Layout.createSequentialGroup()
                                .addComponent(saveBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(clearBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(cancelBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
                            .addGroup(addSensor1Layout.createSequentialGroup()
                                .addComponent(addvendorTele1txt, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addComponent(addvendortele2txt))
                            .addComponent(areavendorTxt1)
                            .addComponent(streetVendorTxt1)
                            .addComponent(nameTxt2)
                            .addComponent(vendorIDTxt1))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addSensor1Layout.setVerticalGroup(
            addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensor1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SensorAddTopic1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vendorIDTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorIdLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorTypeLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameTxt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addSensorvendorLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(streetVendorTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIdLbl4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(areavendorTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIdLbl5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addvendortele2txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addvendorTele1txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(addSensor1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveBtn1)
                    .addComponent(clearBtn1)
                    .addComponent(cancelBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout addVendorPanel1Layout = new javax.swing.GroupLayout(addVendorPanel1);
        addVendorPanel1.setLayout(addVendorPanel1Layout);
        addVendorPanel1Layout.setHorizontalGroup(
            addVendorPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addVendorPanel1Layout.createSequentialGroup()
                .addGap(207, 207, 207)
                .addComponent(addSensor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(259, Short.MAX_VALUE))
        );
        addVendorPanel1Layout.setVerticalGroup(
            addVendorPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addVendorPanel1Layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(addSensor1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(154, Short.MAX_VALUE))
        );

        cardPanel.add(addVendorPanel1, "addVendor");

        manageVendorInfo1.setBackground(new java.awt.Color(204, 204, 204));
        manageVendorInfo1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        manageVendorInfo1.setPreferredSize(new java.awt.Dimension(982, 509));

        vendorDetailsTbl1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        vendorDetailsTbl1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        vendorDetailsTbl1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Vendor ID", "Vendor Name", "Street", "Area", "Contact Number"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        vendorDetailsTbl1.setAutoscrolls(false);
        vendorDetailsTbl1.setBackground(new java.awt.Color(204, 204, 204));
        vendorDetailsTbl1.setRequestFocusEnabled(false);
        vendorDetailsTbl1.setSelectionBackground(new java.awt.Color(153, 153, 153));
        vendorDetailsTbl1.setUpdateSelectionOnSort(false);
        jScrollPane3.setViewportView(vendorDetailsTbl1);

        SensorAddTopic3.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic3.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic3.setText("Vendor Details");
        SensorAddTopic3.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic3.setOpaque(true);

        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        editBtn1.setBackground(new java.awt.Color(153, 153, 153));
        editBtn1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        editBtn1.setText("Edit");
        editBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editBtn1ActionPerformed(evt);
            }
        });

        deleteBtn2.setBackground(new java.awt.Color(153, 153, 153));
        deleteBtn2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        deleteBtn2.setText("Delete");
        deleteBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteBtn2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(deleteBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(editBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(deleteBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout manageVendorInfo1Layout = new javax.swing.GroupLayout(manageVendorInfo1);
        manageVendorInfo1.setLayout(manageVendorInfo1Layout);
        manageVendorInfo1Layout.setHorizontalGroup(
            manageVendorInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageVendorInfo1Layout.createSequentialGroup()
                .addGroup(manageVendorInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(SensorAddTopic3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(manageVendorInfo1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        manageVendorInfo1Layout.setVerticalGroup(
            manageVendorInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(manageVendorInfo1Layout.createSequentialGroup()
                .addComponent(SensorAddTopic3)
                .addGap(9, 9, 9)
                .addGroup(manageVendorInfo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(manageVendorInfo1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
                        .addGap(8, 8, 8))
                    .addGroup(manageVendorInfo1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        cardPanel.add(manageVendorInfo1, "manageVendorInfo");

        editVendorPanel2.setBackground(new java.awt.Color(158, 158, 158));
        editVendorPanel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        editVendorPanel2.setPreferredSize(new java.awt.Dimension(982, 509));

        editVendor2.setBackground(new java.awt.Color(204, 204, 204));
        editVendor2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        editVendorIdLbl6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        editVendorIdLbl6.setText("Vendor ID");

        editvendorTypeLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        editvendorTypeLbl2.setText("Name");

        editVendorvendorLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        editVendorvendorLbl2.setText("Street");

        editVendorTopic5.setBackground(new java.awt.Color(153, 153, 153));
        editVendorTopic5.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        editVendorTopic5.setText("Edit Vendor");
        editVendorTopic5.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        editVendorTopic5.setOpaque(true);

        editVendorIDTxt2.setEditable(false);
        editVendorIDTxt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editVendorIDTxt2ActionPerformed(evt);
            }
        });

        editVendorarearTxt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editVendorarearTxt2ActionPerformed(evt);
            }
        });

        editVendorIdLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        editVendorIdLbl7.setText("Area");

        editVendorIdLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        editVendorIdLbl8.setText("Telephone Number");

        editVendornameTxt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editVendornameTxt3ActionPerformed(evt);
            }
        });

        editVendorsaveBtn2.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        editVendorsaveBtn2.setText("Save");
        editVendorsaveBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editVendorsaveBtn2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout editVendor2Layout = new javax.swing.GroupLayout(editVendor2);
        editVendor2.setLayout(editVendor2Layout);
        editVendor2Layout.setHorizontalGroup(
            editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editVendor2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(editVendorTopic5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, editVendor2Layout.createSequentialGroup()
                        .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(editVendorIdLbl8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editVendorIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editVendorvendorLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editvendorTypeLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editVendorIdLbl6, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(editVendornameTxt3, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(editVendorstreetTxt2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(editVendorarearTxt2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(editVendorIDTxt2)
                            .addGroup(editVendor2Layout.createSequentialGroup()
                                .addComponent(editVendortele1txt1, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                                .addComponent(editVendortele1txt, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editVendor2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(editVendorsaveBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        editVendor2Layout.setVerticalGroup(
            editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editVendor2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editVendorTopic5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editVendorIDTxt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendorIdLbl6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editvendorTypeLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendornameTxt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editVendorvendorLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendorstreetTxt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editVendorIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendorarearTxt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editVendor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editVendorIdLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendortele1txt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editVendortele1txt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editVendorsaveBtn2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout editVendorPanel2Layout = new javax.swing.GroupLayout(editVendorPanel2);
        editVendorPanel2.setLayout(editVendorPanel2Layout);
        editVendorPanel2Layout.setHorizontalGroup(
            editVendorPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editVendorPanel2Layout.createSequentialGroup()
                .addGap(171, 171, 171)
                .addComponent(editVendor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(199, Short.MAX_VALUE))
        );
        editVendorPanel2Layout.setVerticalGroup(
            editVendorPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editVendorPanel2Layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(editVendor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(182, Short.MAX_VALUE))
        );

        cardPanel.add(editVendorPanel2, "editVendor");

        searchPanel2.setBackground(new java.awt.Color(158, 158, 158));
        searchPanel2.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        searchPanel2.setPreferredSize(new java.awt.Dimension(982, 509));

        addSensor2.setBackground(new java.awt.Color(204, 204, 204));
        addSensor2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        SensorAddTopic4.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic4.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic4.setText("Search Vendor");
        SensorAddTopic4.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic4.setOpaque(true);

        searchByLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchByLbl.setText("SEARCH BY");

        jXPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jXPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        vendorLbl2.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(vendorLbl2);
        vendorLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        vendorLbl2.setText("Sensor ID");
        vendorLbl2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vendorLbl2ActionPerformed(evt);
            }
        });

        vendorLbl1.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(vendorLbl1);
        vendorLbl1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        vendorLbl1.setText("Location");
        vendorLbl1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vendorLbl1ActionPerformed(evt);
            }
        });

        vendorLbl.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(vendorLbl);
        vendorLbl.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        vendorLbl.setText("Vendor");
        vendorLbl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vendorLblActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jXPanel1Layout = new javax.swing.GroupLayout(jXPanel1);
        jXPanel1.setLayout(jXPanel1Layout);
        jXPanel1Layout.setHorizontalGroup(
            jXPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jXPanel1Layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addGroup(jXPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(vendorLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vendorLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vendorLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        jXPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {vendorLbl, vendorLbl1, vendorLbl2});

        jXPanel1Layout.setVerticalGroup(
            jXPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jXPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(vendorLbl)
                .addGap(3, 3, 3)
                .addComponent(vendorLbl2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vendorLbl1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jXPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {vendorLbl, vendorLbl1, vendorLbl2});

        javax.swing.GroupLayout SearchByjPanel1Layout = new javax.swing.GroupLayout(SearchByjPanel1);
        SearchByjPanel1.setLayout(SearchByjPanel1Layout);
        SearchByjPanel1Layout.setHorizontalGroup(
            SearchByjPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchByjPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(searchByLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, SearchByjPanel1Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addComponent(jXPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );
        SearchByjPanel1Layout.setVerticalGroup(
            SearchByjPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchByjPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(searchByLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jXPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        addSensorvendorLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addSensorvendorLbl2.setText("SEARCH RESULT");

        searchResultCardPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchResultCardPanel.setLayout(new java.awt.CardLayout());

        vendorSearchResult.setBackground(new java.awt.Color(204, 204, 204));

        searchvendorrIdLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrIdLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrIdLbl7.setText("Vendor ID");

        searchvendorStreetLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorStreetLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorStreetLbl7.setText("Street");

        searchvendorrNameLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrNameLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrNameLbl7.setText("Vendor Name");

        searchvendorrTeleLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrTeleLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrTeleLbl7.setText("Telephone Num");

        searchvendorrAreaLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrAreaLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrAreaLbl7.setText("Area");

        searchResultvendorrStreetLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchResultvendorrStreetLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultvendorrNameLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchResultvendorrNameLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultvendorrtele1Lbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchResultvendorrtele1Lbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultvendorrAreaLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchResultvendorrAreaLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultvendorrIdLbl7.setBackground(new java.awt.Color(204, 204, 204));
        searchResultvendorrIdLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultvendortele2Lbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        javax.swing.GroupLayout vendorSearchResultLayout = new javax.swing.GroupLayout(vendorSearchResult);
        vendorSearchResult.setLayout(vendorSearchResultLayout);
        vendorSearchResultLayout.setHorizontalGroup(
            vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vendorSearchResultLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchvendorrIdLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrNameLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorStreetLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrAreaLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrTeleLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchResultvendorrStreetLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchResultvendorrAreaLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addComponent(searchResultvendorrNameLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addComponent(searchResultvendorrIdLbl7, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addGroup(vendorSearchResultLayout.createSequentialGroup()
                        .addComponent(searchResultvendorrtele1Lbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchResultvendortele2Lbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        vendorSearchResultLayout.setVerticalGroup(
            vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vendorSearchResultLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultvendorrIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrNameLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultvendorrNameLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorStreetLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultvendorrStreetLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrAreaLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultvendorrAreaLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchResultvendortele2Lbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(vendorSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchvendorrTeleLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchResultvendorrtele1Lbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16))
        );

        searchResultCardPanel.add(vendorSearchResult, "vendorSearchResult");

        sensorTypeSearchResult.setBackground(new java.awt.Color(204, 204, 204));

        searchvendorrIdLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrIdLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrIdLbl8.setText("Sensor ID");

        searchvendorStreetLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorStreetLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorStreetLbl8.setText("Date Installed");

        searchvendorrNameLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrNameLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrNameLbl8.setText("Sensor Type");

        searchvendorrTeleLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrTeleLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrTeleLbl8.setText("Expected Expiry Date");

        searchvendorrAreaLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrAreaLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrAreaLbl8.setText("Last checked Date");

        searchResultDateInstalledTxt.setBackground(new java.awt.Color(204, 204, 204));
        searchResultDateInstalledTxt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultSensorTypeTxt.setBackground(new java.awt.Color(204, 204, 204));
        searchResultSensorTypeTxt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultExpectedExpiryDateTxt.setBackground(new java.awt.Color(204, 204, 204));
        searchResultExpectedExpiryDateTxt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLastCheckedDateTxt.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLastCheckedDateTxt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultSensorrIdLbl8.setBackground(new java.awt.Color(204, 204, 204));
        searchResultSensorrIdLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchvendorrNameLbl10.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrNameLbl10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrNameLbl10.setText("Sensor Status");

        searchResultSensorStatusTxt.setBackground(new java.awt.Color(204, 204, 204));
        searchResultSensorStatusTxt.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        javax.swing.GroupLayout sensorTypeSearchResultLayout = new javax.swing.GroupLayout(sensorTypeSearchResult);
        sensorTypeSearchResult.setLayout(sensorTypeSearchResultLayout);
        sensorTypeSearchResultLayout.setHorizontalGroup(
            sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorTypeSearchResultLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchvendorrIdLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrNameLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorStreetLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrAreaLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrTeleLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchResultDateInstalledTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchResultLastCheckedDateTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addComponent(searchResultSensorrIdLbl8, javax.swing.GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                    .addGroup(sensorTypeSearchResultLayout.createSequentialGroup()
                        .addComponent(searchResultSensorTypeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(searchvendorrNameLbl10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchResultSensorStatusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(searchResultExpectedExpiryDateTxt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        sensorTypeSearchResultLayout.setVerticalGroup(
            sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorTypeSearchResultLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrIdLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultSensorrIdLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sensorTypeSearchResultLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(searchvendorrNameLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchResultSensorTypeTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sensorTypeSearchResultLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(searchResultSensorStatusTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(searchvendorrNameLbl10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorStreetLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultDateInstalledTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrAreaLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultLastCheckedDateTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(sensorTypeSearchResultLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrTeleLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultExpectedExpiryDateTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        searchResultCardPanel.add(sensorTypeSearchResult, "sensorTypeSearchResult");

        locationSearchResult1.setBackground(new java.awt.Color(204, 204, 204));

        searchvendorrIdLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrIdLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrIdLbl9.setText("Location ID");

        searchvendorStreetLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorStreetLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorStreetLbl9.setText("Street");

        searchvendorrNameLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrNameLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrNameLbl9.setText("Location Name");

        searchvendorrTeleLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrTeleLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrTeleLbl9.setText("Latitude");

        searchvendorrAreaLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrAreaLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrAreaLbl9.setText("Area");

        searchResultLocationrStreetLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLocationrStreetLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLocationrNameLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLocationrNameLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLocationLatitudeLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLocationLatitudeLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLocationAreaLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLocationAreaLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLocationIdLbl9.setBackground(new java.awt.Color(204, 204, 204));
        searchResultLocationIdLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchResultLocationLogitudeLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N

        searchvendorrTeleLbl10.setBackground(new java.awt.Color(204, 204, 204));
        searchvendorrTeleLbl10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        searchvendorrTeleLbl10.setText("Logitude");

        javax.swing.GroupLayout locationSearchResult1Layout = new javax.swing.GroupLayout(locationSearchResult1);
        locationSearchResult1.setLayout(locationSearchResult1Layout);
        locationSearchResult1Layout.setHorizontalGroup(
            locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(locationSearchResult1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(searchvendorrIdLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrNameLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorStreetLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrAreaLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchvendorrTeleLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchResultLocationrStreetLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchResultLocationAreaLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchResultLocationrNameLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(searchResultLocationIdLbl9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(locationSearchResult1Layout.createSequentialGroup()
                        .addComponent(searchResultLocationLatitudeLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(searchvendorrTeleLbl10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(37, 37, 37)
                        .addComponent(searchResultLocationLogitudeLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        locationSearchResult1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {searchResultLocationLatitudeLbl9, searchResultLocationLogitudeLbl9});

        locationSearchResult1Layout.setVerticalGroup(
            locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(locationSearchResult1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrIdLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultLocationIdLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrNameLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultLocationrNameLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorStreetLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultLocationrStreetLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchvendorrAreaLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchResultLocationAreaLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchResultLocationLogitudeLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(locationSearchResult1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(searchvendorrTeleLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchResultLocationLatitudeLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(searchvendorrTeleLbl10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16))
        );

        locationSearchResult1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {searchResultLocationLatitudeLbl9, searchResultLocationLogitudeLbl9});

        searchResultCardPanel.add(locationSearchResult1, "locationSearchResult");

        javax.swing.GroupLayout SearchResultjPanel2Layout = new javax.swing.GroupLayout(SearchResultjPanel2);
        SearchResultjPanel2.setLayout(SearchResultjPanel2Layout);
        SearchResultjPanel2Layout.setHorizontalGroup(
            SearchResultjPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchResultjPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(addSensorvendorLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(SearchResultjPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(SearchResultjPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(searchResultCardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 608, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        SearchResultjPanel2Layout.setVerticalGroup(
            SearchResultjPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchResultjPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addSensorvendorLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(203, Short.MAX_VALUE))
            .addGroup(SearchResultjPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(SearchResultjPanel2Layout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addComponent(searchResultCardPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        addSensorvendorLbl3.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addSensorvendorLbl3.setText("SEARCH");

        searchCardPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        searchCardPanel.setLayout(new java.awt.CardLayout());

        vendorSearchPanel.setBackground(new java.awt.Color(204, 204, 204));

        vendorIdLbl9.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        vendorIdLbl9.setText("Vendor");

        searchBtnVendor.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        searchBtnVendor.setText("SEARCH");
        searchBtnVendor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBtnVendorActionPerformed(evt);
            }
        });

        jComboBox1searchvendor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout vendorSearchPanelLayout = new javax.swing.GroupLayout(vendorSearchPanel);
        vendorSearchPanel.setLayout(vendorSearchPanelLayout);
        vendorSearchPanelLayout.setHorizontalGroup(
            vendorSearchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, vendorSearchPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(vendorSearchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(vendorSearchPanelLayout.createSequentialGroup()
                        .addGap(0, 207, Short.MAX_VALUE)
                        .addComponent(searchBtnVendor, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36))
                    .addGroup(vendorSearchPanelLayout.createSequentialGroup()
                        .addComponent(vendorIdLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(jComboBox1searchvendor, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        vendorSearchPanelLayout.setVerticalGroup(
            vendorSearchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vendorSearchPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(vendorSearchPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vendorIdLbl9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1searchvendor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchBtnVendor)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        searchCardPanel.add(vendorSearchPanel, "vendorSearch");

        sensorTypeSearchPanel1.setBackground(new java.awt.Color(204, 204, 204));

        sensoTypeLbl10.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensoTypeLbl10.setText("Sensor ID");

        searchSensorBtn.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        searchSensorBtn.setText("SEARCH");
        searchSensorBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchSensorBtnActionPerformed(evt);
            }
        });

        searchsensoridjComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout sensorTypeSearchPanel1Layout = new javax.swing.GroupLayout(sensorTypeSearchPanel1);
        sensorTypeSearchPanel1.setLayout(sensorTypeSearchPanel1Layout);
        sensorTypeSearchPanel1Layout.setHorizontalGroup(
            sensorTypeSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, sensorTypeSearchPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sensorTypeSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(searchSensorBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(sensorTypeSearchPanel1Layout.createSequentialGroup()
                        .addComponent(sensoTypeLbl10, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchsensoridjComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36))
        );
        sensorTypeSearchPanel1Layout.setVerticalGroup(
            sensorTypeSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorTypeSearchPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(sensorTypeSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensoTypeLbl10, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchsensoridjComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchSensorBtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        searchCardPanel.add(sensorTypeSearchPanel1, "sensorTypeSearch");

        locationSearchPanel1.setBackground(new java.awt.Color(204, 204, 204));

        LocationIdLbl11.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        LocationIdLbl11.setText("Location");

        searchBtnLocation.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        searchBtnLocation.setText("SEARCH");
        searchBtnLocation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBtnLocationActionPerformed(evt);
            }
        });

        jComboBox1Searchloc.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout locationSearchPanel1Layout = new javax.swing.GroupLayout(locationSearchPanel1);
        locationSearchPanel1.setLayout(locationSearchPanel1Layout);
        locationSearchPanel1Layout.setHorizontalGroup(
            locationSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, locationSearchPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(locationSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(locationSearchPanel1Layout.createSequentialGroup()
                        .addGap(0, 207, Short.MAX_VALUE)
                        .addComponent(searchBtnLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(locationSearchPanel1Layout.createSequentialGroup()
                        .addComponent(LocationIdLbl11, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(jComboBox1Searchloc, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(36, 36, 36))
        );
        locationSearchPanel1Layout.setVerticalGroup(
            locationSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(locationSearchPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(locationSearchPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LocationIdLbl11, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1Searchloc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchBtnLocation)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        searchCardPanel.add(locationSearchPanel1, "locationSearch");

        javax.swing.GroupLayout SearchjPanel6Layout = new javax.swing.GroupLayout(SearchjPanel6);
        SearchjPanel6.setLayout(SearchjPanel6Layout);
        SearchjPanel6Layout.setHorizontalGroup(
            SearchjPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchjPanel6Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(addSensorvendorLbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(SearchjPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(SearchjPanel6Layout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(searchCardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(20, Short.MAX_VALUE)))
        );
        SearchjPanel6Layout.setVerticalGroup(
            SearchjPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SearchjPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addSensorvendorLbl3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(148, Short.MAX_VALUE))
            .addGroup(SearchjPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(SearchjPanel6Layout.createSequentialGroup()
                    .addGap(41, 41, 41)
                    .addComponent(searchCardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(26, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout addSensor2Layout = new javax.swing.GroupLayout(addSensor2);
        addSensor2.setLayout(addSensor2Layout);
        addSensor2Layout.setHorizontalGroup(
            addSensor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensor2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(addSensor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addSensor2Layout.createSequentialGroup()
                        .addGroup(addSensor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(SensorAddTopic4, javax.swing.GroupLayout.PREFERRED_SIZE, 627, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(addSensor2Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(SearchByjPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(SearchjPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(0, 1, Short.MAX_VALUE))
                    .addComponent(SearchResultjPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        addSensor2Layout.setVerticalGroup(
            addSensor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSensor2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SensorAddTopic4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSensor2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SearchByjPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SearchjPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(SearchResultjPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout searchPanel2Layout = new javax.swing.GroupLayout(searchPanel2);
        searchPanel2.setLayout(searchPanel2Layout);
        searchPanel2Layout.setHorizontalGroup(
            searchPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchPanel2Layout.createSequentialGroup()
                .addContainerGap(194, Short.MAX_VALUE)
                .addComponent(addSensor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(136, 136, 136))
        );
        searchPanel2Layout.setVerticalGroup(
            searchPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(addSensor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        cardPanel.add(searchPanel2, "search");

        editSensorPanel1.setBackground(new java.awt.Color(158, 158, 158));
        editSensorPanel1.setFont(new java.awt.Font("Segoe UI", 0, 11)); // NOI18N
        editSensorPanel1.setPreferredSize(new java.awt.Dimension(982, 509));

        editSensor3.setBackground(new java.awt.Color(204, 204, 204));
        editSensor3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.darkGray, null, null));

        sensorIdLbl6.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl6.setText("Sensor ID");

        sensorTypeLbl2.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorTypeLbl2.setText("Sensor Type");

        sensorExpiryLbl1.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorExpiryLbl1.setText("Expected Expiry Date");

        addSensorvendorLbl4.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        addSensorvendorLbl4.setText("Vendor");

        saveBtn2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        saveBtn2.setText("Save");
        saveBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveBtn2ActionPerformed(evt);
            }
        });

        SensorAddTopic5.setBackground(new java.awt.Color(153, 153, 153));
        SensorAddTopic5.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        SensorAddTopic5.setText("Edit Sensor");
        SensorAddTopic5.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        SensorAddTopic5.setOpaque(true);

        sensorIDTxt1.setEditable(false);
        sensorIDTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensorIDTxt1ActionPerformed(evt);
            }
        });

        addSensorVendorTxt1.setEditable(false);
        addSensorVendorTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSensorVendorTxt1ActionPerformed(evt);
            }
        });

        addSensorDate1.setEditable(false);
        addSensorDate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSensorDate1ActionPerformed(evt);
            }
        });

        sensorIdLbl7.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl7.setText("Location");

        addSensorLocTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSensorLocTxt1ActionPerformed(evt);
            }
        });

        sensorIdLbl8.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        sensorIdLbl8.setText("Sensor Status");

        sensortypeTxt2.setEditable(false);
        sensortypeTxt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sensortypeTxt2ActionPerformed(evt);
            }
        });

        buttonGroup1.add(editSensorRadioButton1);
        editSensorRadioButton1.setText("Error");

        buttonGroup1.add(EditSensorRadioButton2);
        EditSensorRadioButton2.setText("Running");

        javax.swing.GroupLayout editSensor3Layout = new javax.swing.GroupLayout(editSensor3);
        editSensor3.setLayout(editSensor3Layout);
        editSensor3Layout.setHorizontalGroup(
            editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSensor3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SensorAddTopic5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(editSensor3Layout.createSequentialGroup()
                        .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sensorIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addSensorvendorLbl4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorTypeLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sensorIdLbl6, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(editSensor3Layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(sensorIDTxt1))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editSensor3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(saveBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(editSensor3Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(addSensorVendorTxt1)
                                    .addComponent(sensortypeTxt2)
                                    .addComponent(addSensorLocTxt1)
                                    .addComponent(addSensorDate1, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                                    .addGroup(editSensor3Layout.createSequentialGroup()
                                        .addComponent(EditSensorRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(editSensor3Layout.createSequentialGroup()
                        .addComponent(sensorExpiryLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(editSensor3Layout.createSequentialGroup()
                        .addComponent(sensorIdLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(editSensorRadioButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );
        editSensor3Layout.setVerticalGroup(
            editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSensor3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(SensorAddTopic5)
                .addGap(18, 18, 18)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIDTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorIdLbl6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorTypeLbl2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensortypeTxt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addSensorvendorLbl4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSensorVendorTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIdLbl7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addSensorLocTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sensorIdLbl8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editSensorRadioButton1)
                    .addComponent(EditSensorRadioButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSensor3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addSensorDate1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sensorExpiryLbl1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(saveBtn2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        editSensor3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {EditSensorRadioButton2, editSensorRadioButton1});

        javax.swing.GroupLayout editSensorPanel1Layout = new javax.swing.GroupLayout(editSensorPanel1);
        editSensorPanel1.setLayout(editSensorPanel1Layout);
        editSensorPanel1Layout.setHorizontalGroup(
            editSensorPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSensorPanel1Layout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addComponent(editSensor3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(229, Short.MAX_VALUE))
        );
        editSensorPanel1Layout.setVerticalGroup(
            editSensorPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSensorPanel1Layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(editSensor3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );

        cardPanel.add(editSensorPanel1, "editSensor");

        javax.swing.GroupLayout sensorDetailsLayout = new javax.swing.GroupLayout(sensorDetails);
        sensorDetails.setLayout(sensorDetailsLayout);
        sensorDetailsLayout.setHorizontalGroup(
            sensorDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorDetailsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(cardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        sensorDetailsLayout.setVerticalGroup(
            sensorDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sensorDetailsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(cardPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(SensorTaskContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sensorDetails, javax.swing.GroupLayout.DEFAULT_SIZE, 984, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(SensorTaskContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sensorDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sensorDetailsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorDetailsBtnActionPerformed
        // TODO add your handling code here:
        this.setTableSensors(sensorda.getsensorList());

        CardLayout card = (CardLayout) cardPanel.getLayout();
        card.show(cardPanel, "manageSensorInfo");
    }//GEN-LAST:event_sensorDetailsBtnActionPerformed

    private void deleteBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtnActionPerformed
        // TODO add your handling code here:
        if (userhandler.getLoggedInUser() != null) {
            if (userhandler.getLoggedInUser().getAccessLevel() == "A") {
                this.delete();
            } else {
                JOptionPane.showMessageDialog(this, "No access to delete data");
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Invalid login");
            return;
        }
    }//GEN-LAST:event_deleteBtnActionPerformed

    private void sensorAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorAddBtnActionPerformed
        try {
            loadDetails();
            // TODO add your handling code here:
            int sensorID = sensorda.getLastSensorId();
            if (sensorID > 0) {
                sensorIDTxt.setText(sensorID + 1 + "");;
            } else {
                sensorIDTxt.setText(String.valueOf(1));
            }

            CardLayout card = (CardLayout) cardPanel.getLayout();
            card.show(cardPanel, "addSensor");
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_sensorAddBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        // TODO add your handling code here:
        this.setTableSensors(sensorda.getsensorList());
        CardLayout card = (CardLayout) cardPanel.getLayout();
        card.show(cardPanel, "manageSensorInfo");

       loadDetails();
        addSensorDate.setDate(null);
        sensortypeTxt1.setText(null);
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void sensorIDTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorIDTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sensorIDTxtActionPerformed

    private void addSensorDateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSensorDateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addSensorDateActionPerformed

    private void sensorSearchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorSearchBtnActionPerformed
        // TODO add your handling code here:
        CardLayout card = (CardLayout) cardPanel.getLayout();
        card.show(cardPanel, "search");
    }//GEN-LAST:event_sensorSearchBtnActionPerformed

    private void saveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtnActionPerformed
        try {
            if (!validateName(sensortypeTxt1.getText())) {
               sensortypeTxt1.setText("");
               sensortypeTxt1.requestFocus();
              return;
          }
//            if (!validateName(addSensorVendorTxt.getText())) {
//                addSensorVendorTxt.setText("");
//                addSensorVendorTxt.requestFocus();
//                return;
//            }
//            if (!validateName(addSensorLocTxt.getText())) {
//                addSensorLocTxt.setText("");
//                addSensorLocTxt.requestFocus();
//                return;
//            }
            
            if(addvendorComboBoxModel1.getSelectedItem()==null){
                JOptionPane.showMessageDialog(this, "Select the vendor");
                return;
            }
            if(addlocComboBoxModel1.getSelectedItem()==null){
                JOptionPane.showMessageDialog(this, "Select the location");
                return;
            }
            if((!addsensorjRadioButton1.isSelected())&&(!addsensorjRadioButton2.isSelected())){
                JOptionPane.showMessageDialog(this, "Select the status");
                return;
            }
            
            if(addSensorDate.getDate()==null){
                JOptionPane.showMessageDialog(this, "Set the date");
                return;
            }
            addSensor();
            sensorIDTxt.setText(sensorda.getLastSensorId() +1+ "");
            addsensorjRadioButton1.setSelected(false);
            addsensorjRadioButton2.setSelected(false);
            sensortypeTxt1.setText(null);
            loadDetails();
            addSensorDate.setDate(null);
            this.setTableSensors(sensorda.getsensorList());
            CardLayout card = (CardLayout) cardPanel.getLayout();
            card.show(cardPanel, "manageSensorDetails");
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_saveBtnActionPerformed

    private void clearBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearBtnActionPerformed
        try {
            // TODO add your handling code here:
            sensorIDTxt.setText(sensorda.getLastSensorId() + 1 + "");

          loadDetails();
          sensortypeTxt1.setText(null);
            addSensorDate.setDate(null);
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_clearBtnActionPerformed

    private void vendorDetailsBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vendorDetailsBtn2ActionPerformed
        // TODO add your handling code here:
        Logger.getLogger(SensorUI.class.getName()).log(Level.INFO, "vendorDetailsBtn actionperformeed");
        this.setTableVendors(vendorda.getVendorList());
        Logger.getLogger(SensorUI.class.getName()).log(Level.INFO, "setTableVendors method invoked");
        CardLayout card = (CardLayout) cardPanel.getLayout();
        card.show(cardPanel, "manageVendorInfo");
    }//GEN-LAST:event_vendorDetailsBtn2ActionPerformed

    private void addVendorbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addVendorbtnActionPerformed
        try {
            // TODO add your handling code here:

            int vendorID = vendorda.getLastVendorId();
            if (vendorID > 0) {
                vendorIDTxt1.setText(vendorID + 1 + "");
            } else {
                vendorIDTxt1.setText(String.valueOf(1));
            }
            addVendorClear();
            CardLayout card = (CardLayout) cardPanel.getLayout();
            card.show(cardPanel, "addVendor");
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_addVendorbtnActionPerformed

    private void saveBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtn1ActionPerformed

        try {
            ArrayList telephoneNum = new ArrayList();
            Vendor vendor = new Vendor();
            vendor.setVendorId(vendorda.getLastVendorId());
            boolean isValid = false;

            if (!validateName(streetVendorTxt1.getText())) {
                streetVendorTxt1.setText("");
                streetVendorTxt1.requestFocus();
                return;
            }
            if (!validateName(areavendorTxt1.getText())) {
                areavendorTxt1.setText("");
                areavendorTxt1.requestFocus();
                return;
            }

            if (!addvendorTele1txt.getText().equals("")) {
                String telNo = addvendorTele1txt.getText();
                if ((telNo.length() == 10)) {
                    try {
                        Long.parseLong(telNo);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                        addvendorTele1txt.setText("");
                        addvendorTele1txt.requestFocus();
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                    addvendorTele1txt.setText("");
                    addvendorTele1txt.requestFocus();
                    return;
                }
                telephoneNum.add(addvendorTele1txt.getText());

            } else {
                addvendorTele1txt.requestFocus();
                return;
            }
            if (!addvendortele2txt.getText().equals("")) {
                String telNo1 = addvendortele2txt.getText();
                if ((telNo1.length() == 10)) {
                    try {
                        Long.parseLong(telNo1);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                        addvendortele2txt.setText("");
                        addvendortele2txt.requestFocus();
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                    addvendortele2txt.setText("");
                    addvendortele2txt.requestFocus();
                    return;
                }
                telephoneNum.add(addvendortele2txt.getText());
            }
            // TODO add your handling code here:
            this.addVendor(telephoneNum);
            vendorIDTxt1.setText(vendorda.getLastVendorId() + 1 + "");
            streetVendorTxt1.setText(null);
            areavendorTxt1.setText(null);
            nameTxt2.setText(null);
            addvendorTele1txt.setText(null);
            addvendortele2txt.setText(null);
        } catch (SQLException ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_saveBtn1ActionPerformed


    private void clearBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearBtn1ActionPerformed
        // TODO add your handling code here:

        //vendorIDTxt1.setText("");
        streetVendorTxt1.setText(null);
        areavendorTxt1.setText(null);
        nameTxt2.setText(null);
        addvendorTele1txt.setText(null);
        addvendortele2txt.setText(null);
    }//GEN-LAST:event_clearBtn1ActionPerformed

    private void cancelBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtn1ActionPerformed
        // TODO add your handling code here:
        CardLayout card = (CardLayout) cardPanel.getLayout();
        card.show(cardPanel, "manageVendorInfo");
        addVendorClear();

    }//GEN-LAST:event_cancelBtn1ActionPerformed

    private void vendorIDTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vendorIDTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_vendorIDTxt1ActionPerformed

    private void areavendorTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_areavendorTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_areavendorTxt1ActionPerformed

    private void nameTxt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameTxt2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameTxt2ActionPerformed

    private void editBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBtn1ActionPerformed
        // TODO add your handling code here:
        if (userhandler.getLoggedInUser() != null) {

            if (userhandler.getLoggedInUser().getAccessLevel() == "A" || userhandler.getLoggedInUser().getAccessLevel() == "M") {
                if (vendorDetailsTbl1.getSelectedRow() > -1) {
                    try {
                        List<Integer> tele = new ArrayList<>();
                        System.out.println("delete btn");
                        int i = vendorDetailsTbl1.getSelectedRow();
                        int l = Integer.parseInt(vendorDetailsTbl1.getValueAt(i, 0).toString());
                        Vendor vendor = new Vendor();
                        vendor = vendorda.getVendorByID(l);
                        editVendorIDTxt2.setText(l + "");
                        editVendornameTxt3.setText(vendor.getVendorName());
                        editVendorstreetTxt2.setText(vendor.getStreet());
                        editVendorarearTxt2.setText(vendor.getArea());
                        tele = vendor.getTelephoneNum();
                        editVendortele1txt1.setText(tele.get(0) + "");
                        if (tele.size() == 2) {
                            editVendortele1txt.setText(tele.get(1) + "");
                        }
                        CardLayout card = (CardLayout) cardPanel.getLayout();
                        card.show(cardPanel, "editVendor");
                    } catch (SQLException ex) {
                        Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Select the row you want to edit");
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(this, "You have no access to edit this records.");
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Invalid login");
            return;
        }


    }//GEN-LAST:event_editBtn1ActionPerformed

    private void deleteBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteBtn2ActionPerformed
        // TODO add your handling code here:
        if (userhandler.getLoggedInUser() != null) {
            if (userhandler.getLoggedInUser().getAccessLevel() == "A") {
                if (vendorDetailsTbl1.getSelectedRow() > -1) {
                    System.out.println("delete btn");
                    int i = vendorDetailsTbl1.getSelectedRow();
                    String message = "Are you sure you want delete?";
                    String title = "Delete";
                    // display the JOptionPane showConfirmDialog
                    int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.YES_OPTION) {
                        vendorda.deleteVendor(Integer.parseInt(vendorDetailsTbl1.getValueAt(i, 0).toString()));
                        // vendorDetailsTableModel.setNumRows(0);
                        this.setTableVendors(vendorda.getVendorList());
                    } else {
                        return;
                    }

                } else {
                    JOptionPane.showMessageDialog(this, "Select the row you want to delete");
                    return;
            // vendorDetailsTableModel.setNumRows(0);
                    //  this.setTableVendors(vendorda.getVendorList());
                }
            } else {
                JOptionPane.showMessageDialog(this, "No access to delete data.");
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Invalid login");
            return;
        }
    }//GEN-LAST:event_deleteBtn2ActionPerformed

    private void editVendorsaveBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editVendorsaveBtn2ActionPerformed
        ArrayList telephoneNum = new ArrayList();
        Vendor vendor = new Vendor();
        vendor.setVendorId(Integer.parseInt(editVendorIDTxt2.getText()));
        vendor.setVendorName(editVendornameTxt3.getText());
        vendor.setStreet(editVendorstreetTxt2.getText());
        vendor.setArea(editVendorarearTxt2.getText());
        if (!editVendortele1txt1.getText().equals("")) {
            String telNo = editVendortele1txt1.getText();
            if ((telNo.length() == 10)) {
                try {
                    Long.parseLong(telNo);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                    editVendortele1txt1.setText("");
                    editVendortele1txt1.requestFocus();
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                editVendortele1txt1.setText("");
                editVendortele1txt1.requestFocus();
                return;
            }
            telephoneNum.add(editVendortele1txt.getText());

        } else {
            editVendortele1txt1.requestFocus();
            return;
        }
        if (!editVendortele1txt.getText().equals("")) {
            String telNo1 = editVendortele1txt.getText();
            if ((telNo1.length() == 10)) {
                try {
                    Long.parseLong(telNo1);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                    editVendortele1txt.setText("");
                    editVendortele1txt.requestFocus();
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(sensorDetails, "Enter a valid telephone number");
                editVendortele1txt.setText("");
                editVendortele1txt.requestFocus();
                return;
            }
            telephoneNum.add(editVendortele1txt1.getText());
        }
        vendor.setTelephoneNum(telephoneNum);
        String message = "Are you sure you need to save the changes";
        String title = "Confirm Dialog";
        // display the JOptionPane showConfirmDialog
        int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            vendorda.updateVendor(vendor, telephoneNum);
            this.setTableVendors(vendorda.getVendorList());
            CardLayout card = (CardLayout) cardPanel.getLayout();
            card.show(cardPanel, "manageVendorInfo");
        }

        editVendorIDTxt2.setText(null);
        editVendorstreetTxt2.setText(null);
        editVendorarearTxt2.setText(null);
        editVendornameTxt3.setText(null);
        editVendortele1txt.setText(null);
        editVendortele1txt1.setText(null);


    }//GEN-LAST:event_editVendorsaveBtn2ActionPerformed

    private void editVendorIDTxt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editVendorIDTxt2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editVendorIDTxt2ActionPerformed

    private void editVendorarearTxt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editVendorarearTxt2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editVendorarearTxt2ActionPerformed

    private void editVendornameTxt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editVendornameTxt3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editVendornameTxt3ActionPerformed

    private void vendorLblActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vendorLblActionPerformed
        // TODO add your handling code here:
        clearAll();
        CardLayout card1 = (CardLayout) searchCardPanel.getLayout();
        card1.show(searchCardPanel, "vendorSearch");
        CardLayout card = (CardLayout) searchResultCardPanel.getLayout();
        card.show(searchResultCardPanel, "vendorSearchResult");


    }//GEN-LAST:event_vendorLblActionPerformed

    private void vendorLbl1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vendorLbl1ActionPerformed
        // TODO add your handling code here:
        clearAll();
        CardLayout card1 = (CardLayout) searchCardPanel.getLayout();
        card1.show(searchCardPanel, "locationSearch");
        CardLayout card = (CardLayout) searchResultCardPanel.getLayout();
        card.show(searchResultCardPanel, "locationSearchResult");
    }//GEN-LAST:event_vendorLbl1ActionPerformed

    private void vendorLbl2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vendorLbl2ActionPerformed
        // TODO add your handling code here:
        clearAll();
        CardLayout card1 = (CardLayout) searchCardPanel.getLayout();
        card1.show(searchCardPanel, "sensorTypeSearch");
        CardLayout card = (CardLayout) searchResultCardPanel.getLayout();
        card.show(searchResultCardPanel, "sensorTypeSearchResult");
    }//GEN-LAST:event_vendorLbl2ActionPerformed

    private void searchBtnVendorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBtnVendorActionPerformed
        // TODO add your handling code here:
        
                String ven4 = addvendorComboBoxModel.getSelectedItem().toString();
                if(addvendorComboBoxModel.getSelectedItem()==null){
                    JOptionPane.showMessageDialog(this, "Select the vendor");
                    return;
                }
        String l = addvendorComboBoxModel.getSelectedItem().toString();
        
        clearAll();
        this.searchVendor(l);
    }//GEN-LAST:event_searchBtnVendorActionPerformed

    private void searchSensorBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchSensorBtnActionPerformed
        // TODO add your handling code here:
if(addsensorTypeComboBoxModel.getSelectedItem()==null){
    JOptionPane.showMessageDialog(this, "Select the sensor type");
    return;
}
        String l =addsensorTypeComboBoxModel.getSelectedItem().toString();
        clearAll();
        

            this.searchSensorType(l);
        


    }//GEN-LAST:event_searchSensorBtnActionPerformed

    private void searchBtnLocationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBtnLocationActionPerformed
        // TODO add your handling code here:
if(addlocComboBoxModel.getSelectedItem()==null){
    JOptionPane.showMessageDialog(this, "Select the sensor type");
    return;
}
        String l = addlocComboBoxModel.getSelectedItem().toString();
        
        System.out.println(l);
        clearAll();
        this.searchLocation(l);
    }//GEN-LAST:event_searchBtnLocationActionPerformed

    private void saveBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveBtn2ActionPerformed
        try {
            // TODO add your handling code here:
            Sensor sensor = new Sensor();
            sensor.setSensorId(Integer.parseInt(sensorIDTxt1.getText()));
            sensor.setSensorType(sensortypeTxt2.getText());
            if (EditSensorRadioButton2.isSelected() || editSensorRadioButton1.isSelected()) {
                if (EditSensorRadioButton2.isSelected()) {
                    sensor.setSensorStatus("R");
                } else {
                    sensor.setSensorStatus("E");
                }

                Location loc = new Location();
                loc = locationDA.getLocationByName(addSensorLocTxt1.getText());
                if (loc != null) {
                    sensor.setLocationId(loc.getLocationId());

                    String message = "Are you sure you need to save the changes";
                    String title = "Confirm Dialog";
                    // display the JOptionPane showConfirmDialog
                    int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.YES_OPTION) {
                        sensorda.updateSensor(sensor);

                        this.setTableSensors(sensorda.getsensorList());
                        CardLayout card = (CardLayout) cardPanel.getLayout();
                        card.show(cardPanel, "manageSensorInfo");
                    } else {
                        return;
                    }
                    sensorIDTxt1.setText(null);
                    sensortypeTxt2.setText(null);
                    EditSensorRadioButton2.setSelected(false);
                    editSensorRadioButton1.setSelected(false);
                    addSensorLocTxt1.setText(null);
                    addSensorVendorTxt1.setText(null);
                    addSensorDate1.setDate(null);

                } else {
                    JOptionPane.showMessageDialog(sensorDetails, "Enter a valid location");
                    addSensorLocTxt1.setText("");
                    addSensorLocTxt1.requestFocus();
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "Select the status of the sensor");
                EditSensorRadioButton2.requestFocus();
                return;
            }

        } catch (Exception ex) {
            Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_saveBtn2ActionPerformed

    private void sensorIDTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensorIDTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sensorIDTxt1ActionPerformed

    private void addSensorDate1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSensorDate1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addSensorDate1ActionPerformed

    private void addSensorLocTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSensorLocTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addSensorLocTxt1ActionPerformed

    private void sensortypeTxt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensortypeTxt2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sensortypeTxt2ActionPerformed

    private void editBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editBtnActionPerformed
        if (userhandler.getLoggedInUser() != null) {
            if (userhandler.getLoggedInUser().getAccessLevel() == "A" || userhandler.getLoggedInUser().getAccessLevel() == "M") {
                if (SensorDetailsTbl.getSelectedRow() > -1) {
                    try {

                        int i = SensorDetailsTbl.getSelectedRow();
                        int l = Integer.parseInt(SensorDetailsTbl.getValueAt(i, 0).toString());
                        Sensor sensor = new Sensor();
                        sensor = sensorda.getSensorDetails1(l);
                        sensorIDTxt1.setText(sensor.getSensorId() + "");
                        sensortypeTxt2.setText(sensor.getSensorType());
                        Vendor vendor = new Vendor();
                        vendor = vendorda.getVendorByID(sensor.getVendorId());
                        addSensorVendorTxt1.setText(vendor.getVendorName());
                        Location location = new Location();
                        location = locationDA.getLocationByID(sensor.getLocationId());
                        System.out.println("location" + sensor.getLocationId());
                        addSensorLocTxt1.setText(location.getLocationName());
                        if (sensor.getSensorStatus() == "R") {
                            EditSensorRadioButton2.setSelected(true);
                        } else {
                            editSensorRadioButton1.setSelected(true);
                        }

                        addSensorDate1.setDate(sensor.getExpectedExpiryDate());
                        CardLayout card = (CardLayout) cardPanel.getLayout();
                        card.show(cardPanel, "editSensor");
                    } catch (SQLException ex) {
                        Logger.getLogger(SensorUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Select the row you want to edit");
                    return;
                }
            } else {
                JOptionPane.showMessageDialog(this, "You have no access to edit this records.");
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Invalid login");
            return;

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_editBtnActionPerformed

    private void addSensorVendorTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSensorVendorTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addSensorVendorTxt1ActionPerformed

    private void addsensorvendorjComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addsensorvendorjComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addsensorvendorjComboBox2ActionPerformed

    private void addsensorlocjComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addsensorlocjComboBox3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addsensorlocjComboBox3ActionPerformed

    private void sensortypeTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sensortypeTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sensortypeTxt1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SensorUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SensorUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SensorUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SensorUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SensorUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton EditSensorRadioButton2;
    private javax.swing.JLabel LocationIdLbl11;
    private org.jdesktop.swingx.JXTaskPane Search;
    private javax.swing.JPanel SearchByjPanel1;
    private javax.swing.JPanel SearchResultjPanel2;
    private javax.swing.JPanel SearchjPanel6;
    private javax.swing.JLabel SensorAddTopic;
    private javax.swing.JLabel SensorAddTopic1;
    private javax.swing.JLabel SensorAddTopic2;
    private javax.swing.JLabel SensorAddTopic3;
    private javax.swing.JLabel SensorAddTopic4;
    private javax.swing.JLabel SensorAddTopic5;
    private javax.swing.JTable SensorDetailsTbl;
    private org.jdesktop.swingx.JXTaskPaneContainer SensorTaskContainer;
    private javax.swing.JPanel addSensor;
    private javax.swing.JPanel addSensor1;
    private javax.swing.JPanel addSensor2;
    private org.jdesktop.swingx.JXDatePicker addSensorDate;
    private org.jdesktop.swingx.JXDatePicker addSensorDate1;
    private javax.swing.JTextField addSensorLocTxt1;
    private javax.swing.JPanel addSensorPanel;
    private javax.swing.JTextField addSensorVendorTxt1;
    private javax.swing.JLabel addSensorvendorLbl;
    private javax.swing.JLabel addSensorvendorLbl1;
    private javax.swing.JLabel addSensorvendorLbl2;
    private javax.swing.JLabel addSensorvendorLbl3;
    private javax.swing.JLabel addSensorvendorLbl4;
    private javax.swing.JPanel addVendorPanel1;
    private org.jdesktop.swingx.JXButton addVendorbtn;
    private javax.swing.JRadioButton addsensorjRadioButton1;
    private javax.swing.JRadioButton addsensorjRadioButton2;
    private javax.swing.JComboBox addsensorlocjComboBox3;
    private javax.swing.JComboBox addsensorvendorjComboBox2;
    private javax.swing.JTextField addvendorTele1txt;
    private javax.swing.JTextField addvendortele2txt;
    private javax.swing.JTextField areavendorTxt1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton cancelBtn1;
    private javax.swing.JPanel cardPanel;
    private javax.swing.JButton clearBtn;
    private javax.swing.JButton clearBtn1;
    private javax.swing.JButton deleteBtn;
    private javax.swing.JButton deleteBtn2;
    private javax.swing.JButton editBtn;
    private javax.swing.JButton editBtn1;
    private javax.swing.JPanel editSensor3;
    private javax.swing.JPanel editSensorPanel1;
    private javax.swing.JRadioButton editSensorRadioButton1;
    private javax.swing.JPanel editVendor2;
    private javax.swing.JTextField editVendorIDTxt2;
    private javax.swing.JLabel editVendorIdLbl6;
    private javax.swing.JLabel editVendorIdLbl7;
    private javax.swing.JLabel editVendorIdLbl8;
    private javax.swing.JPanel editVendorPanel2;
    private javax.swing.JLabel editVendorTopic5;
    private javax.swing.JTextField editVendorarearTxt2;
    private javax.swing.JTextField editVendornameTxt3;
    private javax.swing.JButton editVendorsaveBtn2;
    private javax.swing.JTextField editVendorstreetTxt2;
    private javax.swing.JTextField editVendortele1txt;
    private javax.swing.JTextField editVendortele1txt1;
    private javax.swing.JLabel editVendorvendorLbl2;
    private javax.swing.JLabel editvendorTypeLbl2;
    private javax.swing.JComboBox jComboBox1Searchloc;
    private javax.swing.JComboBox jComboBox1searchvendor;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    private org.jdesktop.swingx.JXPanel jXPanel1;
    private org.jdesktop.swingx.JXTable jXTable1;
    private javax.swing.JPanel locationSearchPanel1;
    private javax.swing.JPanel locationSearchResult1;
    private javax.swing.JPanel manageSensorInfo;
    private javax.swing.JPanel manageVendorInfo1;
    private javax.swing.JTextField nameTxt2;
    private javax.swing.JButton saveBtn;
    private javax.swing.JButton saveBtn1;
    private javax.swing.JButton saveBtn2;
    private javax.swing.JButton searchBtnLocation;
    private javax.swing.JButton searchBtnVendor;
    private javax.swing.JLabel searchByLbl;
    private org.jdesktop.swingx.JXPanel searchCardPanel;
    private javax.swing.JPanel searchPanel2;
    private org.jdesktop.swingx.JXPanel searchResultCardPanel;
    private javax.swing.JLabel searchResultDateInstalledTxt;
    private javax.swing.JLabel searchResultExpectedExpiryDateTxt;
    private javax.swing.JLabel searchResultLastCheckedDateTxt;
    private javax.swing.JLabel searchResultLocationAreaLbl9;
    private javax.swing.JLabel searchResultLocationIdLbl9;
    private javax.swing.JLabel searchResultLocationLatitudeLbl9;
    private javax.swing.JLabel searchResultLocationLogitudeLbl9;
    private javax.swing.JLabel searchResultLocationrNameLbl9;
    private javax.swing.JLabel searchResultLocationrStreetLbl9;
    private javax.swing.JLabel searchResultSensorStatusTxt;
    private javax.swing.JLabel searchResultSensorTypeTxt;
    private javax.swing.JLabel searchResultSensorrIdLbl8;
    private javax.swing.JLabel searchResultvendorrAreaLbl7;
    private javax.swing.JLabel searchResultvendorrIdLbl7;
    private javax.swing.JLabel searchResultvendorrNameLbl7;
    private javax.swing.JLabel searchResultvendorrStreetLbl7;
    private javax.swing.JLabel searchResultvendorrtele1Lbl7;
    private javax.swing.JLabel searchResultvendortele2Lbl7;
    private javax.swing.JButton searchSensorBtn;
    private javax.swing.JComboBox searchsensoridjComboBox1;
    private javax.swing.JLabel searchvendorStreetLbl7;
    private javax.swing.JLabel searchvendorStreetLbl8;
    private javax.swing.JLabel searchvendorStreetLbl9;
    private javax.swing.JLabel searchvendorrAreaLbl7;
    private javax.swing.JLabel searchvendorrAreaLbl8;
    private javax.swing.JLabel searchvendorrAreaLbl9;
    private javax.swing.JLabel searchvendorrIdLbl7;
    private javax.swing.JLabel searchvendorrIdLbl8;
    private javax.swing.JLabel searchvendorrIdLbl9;
    private javax.swing.JLabel searchvendorrNameLbl10;
    private javax.swing.JLabel searchvendorrNameLbl7;
    private javax.swing.JLabel searchvendorrNameLbl8;
    private javax.swing.JLabel searchvendorrNameLbl9;
    private javax.swing.JLabel searchvendorrTeleLbl10;
    private javax.swing.JLabel searchvendorrTeleLbl7;
    private javax.swing.JLabel searchvendorrTeleLbl8;
    private javax.swing.JLabel searchvendorrTeleLbl9;
    private javax.swing.JLabel sensoTypeLbl10;
    private org.jdesktop.swingx.JXButton sensorAddBtn;
    private javax.swing.JPanel sensorDetails;
    private org.jdesktop.swingx.JXButton sensorDetailsBtn;
    private javax.swing.JLabel sensorExpiryLbl;
    private javax.swing.JLabel sensorExpiryLbl1;
    private javax.swing.JTextField sensorIDTxt;
    private javax.swing.JTextField sensorIDTxt1;
    private javax.swing.JLabel sensorIdLbl;
    private javax.swing.JLabel sensorIdLbl1;
    private javax.swing.JLabel sensorIdLbl2;
    private javax.swing.JLabel sensorIdLbl3;
    private javax.swing.JLabel sensorIdLbl4;
    private javax.swing.JLabel sensorIdLbl5;
    private javax.swing.JLabel sensorIdLbl6;
    private javax.swing.JLabel sensorIdLbl7;
    private javax.swing.JLabel sensorIdLbl8;
    private org.jdesktop.swingx.JXTaskPane sensorManage;
    private org.jdesktop.swingx.JXButton sensorSearchBtn;
    private javax.swing.JLabel sensorTypeLbl;
    private javax.swing.JLabel sensorTypeLbl1;
    private javax.swing.JLabel sensorTypeLbl2;
    private javax.swing.JPanel sensorTypeSearchPanel1;
    private javax.swing.JPanel sensorTypeSearchResult;
    private javax.swing.JTextField sensortypeTxt1;
    private javax.swing.JTextField sensortypeTxt2;
    private javax.swing.JTextField streetVendorTxt1;
    private org.jdesktop.swingx.JXButton vendorDetailsBtn2;
    private javax.swing.JTable vendorDetailsTbl1;
    private javax.swing.JTextField vendorIDTxt1;
    private javax.swing.JLabel vendorIdLbl9;
    private javax.swing.JRadioButton vendorLbl;
    private javax.swing.JRadioButton vendorLbl1;
    private javax.swing.JRadioButton vendorLbl2;
    private org.jdesktop.swingx.JXTaskPane vendorManage;
    private javax.swing.JPanel vendorSearchPanel;
    private javax.swing.JPanel vendorSearchResult;
    // End of variables declaration//GEN-END:variables
}
