/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import dataAccess.user.UserDA;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 *
 * @author ChathurangaKCD
 */
public class UserHandler {

    //<editor-fold defaultstate="collapsed" desc="Singleton code">
    private static UserHandler instance;

    public static UserHandler getInstance() {
        if (instance == null) {
            instance = new UserHandler();
            instance.loggedInUser = new UserDA().getUser("admin");
        }
        return instance;
    }
//</editor-fold>

    private User loggedInUser;

    public enum MESSAGE {

        M_USERNAME_EXISTS,
        M_INVALID_CREDENTIALS,
        M_AN_ERROR_OCCURED,
        M_SUCCESS
    }
    public static final String ACCESS_MAINTAINANCE = "M";
    public static final String ACCESS_ADMIN = "A";
    public static final String ACCESS_GENERAL_USER = "G";
    public static final String ACCESS_REMOVED = "R";
    public static final String ACCESS_MANAGEMENT = "D";

    public static final String USER = "%user%";
    public static final String PASSWORD = "%pass%";

    /**
     * @return the loggedInUser
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

    public MESSAGE login(String username, String Password) {
        try {
            UserDA userDA = new UserDA();
            User user = userDA.getUser(username);
            System.out.println(user);
            if (user.getUsrId() == 0) {
                return MESSAGE.M_INVALID_CREDENTIALS;
            } else if (user.getUsrId() == -1) {
                return MESSAGE.M_AN_ERROR_OCCURED;
            } else {
                if (LoginBL.checkPassword(Password, user.getPassword())) {
                    loggedInUser = user;
                    return MESSAGE.M_SUCCESS;
                } else {
                    return MESSAGE.M_INVALID_CREDENTIALS;
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            return MESSAGE.M_AN_ERROR_OCCURED;
        }
    }
}
