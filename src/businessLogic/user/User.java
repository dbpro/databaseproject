/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import dataAccess.user.LoginDA;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import businessLogic.user.UserHandler.MESSAGE;

/**
 *
 * @author ChathurangaKCD
 */
public abstract class User {

    private Integer usrId;
    private String firstName;
    private String lastName;
    private String nic;
    private String username;
    private String password;
    private String accessLevel;
    private String department;
    private String street;
    private String area;
    private List<String> contactNoList;

    public User() {
    }

    public abstract void refresh();

    public abstract void add();

    public abstract void update();

    public void addLoginDetails() {
        LoginDA login = new LoginDA();
        login.setLogin(this);
    }

    public MESSAGE changePassword(String oldPassword, String newPassword) {
        try {
            LoginDA login = new LoginDA();
            if (LoginBL.checkPassword(oldPassword, this.getPassword())) {
                this.setPassword(LoginBL.hashPassword(newPassword));
                login.editPassword(this);
                return MESSAGE.M_SUCCESS;
            }
            return MESSAGE.M_INVALID_CREDENTIALS;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            return MESSAGE.M_AN_ERROR_OCCURED;
        }
    }

    public MESSAGE changeUsername(String newUsername, String password) {
        LoginDA login = new LoginDA();
        try {
            if (!LoginBL.checkPassword(password, this.getPassword())) {
                return MESSAGE.M_INVALID_CREDENTIALS;
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            return MESSAGE.M_AN_ERROR_OCCURED;
        }
        if (login.isUsernameAvailable(newUsername)) {
            this.setUsername(newUsername);
            login.editUsername(this);
            return MESSAGE.M_SUCCESS;
        } else {
            return MESSAGE.M_USERNAME_EXISTS;
        }
    }

    public User(Integer usrId, String firstName, String lastName, String nic, String username, String password, String accessLevel, String departmet, String street, String area) {
        this.usrId = usrId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nic = nic;
        this.username = username;
        this.password = password;
        this.accessLevel = accessLevel;
        this.department = departmet;
        this.street = street;
        this.area = area;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessLevel() {
        return UserHandler.ACCESS_GENERAL_USER;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public List<String> getContactNoList() {
        return contactNoList;
    }

    public void setContactNoList(List<String> contactNoList) {
        this.contactNoList = contactNoList;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof User) {
            return this.usrId.equals(((User) object).usrId);
        }
        return false;
    }

    @Override
    public String toString() {
        return "businessLogic.Usertb[ usrId=" + usrId + " ]";
    }

}
