/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author chath
 */
@Embeddable
public class ContactNoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "usr_id")
    private int usrId;
    @Basic(optional = false)
    @Column(name = "phone_no")
    private String phoneNo;

    public ContactNoPK() {
    }

    public ContactNoPK(int usrId, String phoneNo) {
        this.usrId = usrId;
        this.phoneNo = phoneNo;
    }

    public int getUsrId() {
        return usrId;
    }

    public void setUsrId(int usrId) {
        this.usrId = usrId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) usrId;
        hash += (phoneNo != null ? phoneNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactNoPK)) {
            return false;
        }
        ContactNoPK other = (ContactNoPK) object;
        if (this.usrId != other.usrId) {
            return false;
        }
        if ((this.phoneNo == null && other.phoneNo != null) || (this.phoneNo != null && !this.phoneNo.equals(other.phoneNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.ContactNoPK[ usrId=" + usrId + ", phoneNo=" + phoneNo + " ]";
    }
    
}
