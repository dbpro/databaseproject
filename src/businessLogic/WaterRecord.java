/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "water_record")
@NamedQueries({
    @NamedQuery(name = "WaterRecord.findAll", query = "SELECT w FROM WaterRecord w")})
public class WaterRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    @Column(name = "water_depth")
    private Integer waterDepth;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ph_level")
    private double phLevel;
    @Column(name = "temperature")
    private double temperature;
    @Column(name = "turbidity")
    private Integer turbidity;
    @Column(name = "pressure")
    private double pressure;
    @Column(name = "hardness")
    private double hardness;
    @Column(name = "BOD_value")
    private double bODvalue;
    @Column(name = "COD_value")
    private double cODvalue;
    @Column(name = "anion_per")
    private double anionPer;
    @Column(name = "cation_per")
    private double cationPer;
    @Column(name = "nitrate_per")
    private double nitratePer;
    @JoinColumn(name = "record_id", referencedColumnName = "record_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Recordtb recordtb;

    public WaterRecord() {
    }

    public WaterRecord(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getWaterDepth() {
        return waterDepth;
    }

    public void setWaterDepth(Integer waterDepth) {
        this.waterDepth = waterDepth;
    }

    public double getPhLevel() {
        return phLevel;
    }

    public void setPhLevel(double phLevel) {
        this.phLevel = phLevel;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public Integer getTurbidity() {
        return turbidity;
    }

    public void setTurbidity(Integer turbidity) {
        this.turbidity = turbidity;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHardness() {
        return hardness;
    }

    public void setHardness(double hardness) {
        this.hardness = hardness;
    }

    public double getBODvalue() {
        return bODvalue;
    }

    public void setBODvalue(double bODvalue) {
        this.bODvalue = bODvalue;
    }

    public double getCODvalue() {
        return cODvalue;
    }

    public void setCODvalue(double cODvalue) {
        this.cODvalue = cODvalue;
    }

    public double getAnionPer() {
        return anionPer;
    }

    public void setAnionPer(double anionPer) {
        this.anionPer = anionPer;
    }

    public double getCationPer() {
        return cationPer;
    }

    public void setCationPer(double cationPer) {
        this.cationPer = cationPer;
    }

    public double getNitratePer() {
        return nitratePer;
    }

    public void setNitratePer(double nitratePer) {
        this.nitratePer = nitratePer;
    }

    public Recordtb getRecordtb() {
        return recordtb;
    }

    public void setRecordtb(Recordtb recordtb) {
        this.recordtb = recordtb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WaterRecord)) {
            return false;
        }
        WaterRecord other = (WaterRecord) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.WaterRecord[ recordId=" + recordId + " ]";
    }
    
}
