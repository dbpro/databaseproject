/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic.user;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ChathurangaKCD
 */
public class InputValidator {

    private static final String PATTERN_EMAIL
            = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String PATTERN_NIC = "^(([0-9]{9})(V|v))$";
    private static final String PATTERN_CONTACT_NO = "^((\\+94|0)([1-9])([0-9]{8}))$";
    /*
     *must contain one digit from 0-9,
     *must contains one lowercase characters
     *must contains one uppercase characters
     *must contains one special symbols in the list "@#$%"
     *length at least 6 characters and maximum of 20
     */
    private static final String PATTERN_PASSWORD = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    public static boolean isValidEmail(String email) {
        return Pattern.matches(PATTERN_EMAIL, email);
    }

    public static boolean isValidNIC(String NIC) {
        return Pattern.matches(PATTERN_NIC, NIC);
    }

    public static boolean isValidPassword(String password) {
        return Pattern.matches(PATTERN_PASSWORD,password);
    }

    public static boolean isValidContactNo(String contactNo) {
        return Pattern.matches(PATTERN_CONTACT_NO, contactNo);
    }

    public static boolean isValidString(String string) {
        return true;
    }
}
