/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewLayer.user;

import businessLogic.user.Maintains;
import businessLogic.user.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author chath
 */
public class MaintainTM extends AbstractTableModel {

    private final String[] columnNames = new String[]{
        "<html>User ID</html>", "<html>Sensor ID</html>", "<html>Rep. date</html>", "<html>Rep. time</html>", "<html>Description</html>"
    };
    private static final int INDEX_USER_ID = 0;
    private static final int INDEX_SENSOR_ID = 1;
    private static final int INDEX_DATE = 2;
    private static final int INDEX_TIME = 3;
    private static final int INDEX_DESC = 4;
    private final DateFormat df;
    private ArrayList<Maintains> list;

    public MaintainTM(ArrayList<Maintains> list) {
        this.list = list;
        df = new SimpleDateFormat("hh:mm:ss aa");
    }

    @Override
    public int getRowCount() {
        return list.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Maintains c = list.get(rowIndex);
        Object returnVal = null;
        switch (columnIndex) {
            case INDEX_SENSOR_ID:
                returnVal = c.getSensorId();
                break;
            case INDEX_USER_ID:
                returnVal = c.getUsrId();
                break;
            case INDEX_DESC:
                returnVal = c.getDescription();
                break;
            case INDEX_DATE:
                returnVal = c.getRepDate();
                break;
            case INDEX_TIME:

                returnVal = df.format(c.getRepTime());
                break;
            default:
                break;
        }
        return returnVal;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (list.isEmpty()) {
            return Object.class;
        }
        return getValueAt(0, columnIndex).getClass();
    }

    public void setList(ArrayList<Maintains> list) {
        this.list = list;
        fireTableDataChanged();
    }

//    public Usertb getCompanyAt(int index) {
//        if (index < users.size()) {
//            return users.get(index);
//        }
//        return null;
//    }
//    public void filter(String userType, int searchBy, String query) {
//        if (userType.equals("")) {
//            usersByType = new ArrayList<>();
//            for (Usertb u : list) {
//                if (u.getAccessLevel().equals(userType)) {
//                    usersByType.add(u);
//                }
//            }
//        } else {
//            usersByType = list;
//        }
//        if (query.equals("")) {
//            users = usersByType;
//        } else if (searchBy == 0) {
//            users = new ArrayList<>();
//            usersByType.stream().filter((u) -> (u.getFirstName().contains(query)||u.getLastName().contains(query))).forEach((u) -> {
//                users.add(u);
//            });
//        } else if (searchBy == 1) {
//            users = new ArrayList<>();
//            usersByType.stream().filter((u) -> (u.getNic().startsWith(query))).forEach((u) -> {
//                users.add(u);
//            });
//        } else {
//            users = new ArrayList<>();
//            usersByType.stream().filter((u) -> (u.getStreet().contains(query)||u.getArea().contains(query))).forEach((u) -> {
//                users.add(u);
//            });
//        }
//        fireTableDataChanged();
//    }
}

