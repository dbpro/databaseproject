/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLogic;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author kanchanaR
 */
@Entity
@Table(name = "illuminance")
@NamedQueries({
    @NamedQuery(name = "Illuminance.findAll", query = "SELECT i FROM Illuminance i")})
public class Illuminance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "record_id")
    private Integer recordId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "luminacy")
    private Float luminacy;
    @Column(name = "sample_height")
    private Integer sampleHeight;
    @JoinColumn(name = "record_id", referencedColumnName = "record_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Recordtb recordtb;

    public Illuminance() {
    }

    public Illuminance(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Float getLuminacy() {
        return luminacy;
    }

    public void setLuminacy(Float luminacy) {
        this.luminacy = luminacy;
    }

    public Integer getSampleHeight() {
        return sampleHeight;
    }

    public void setSampleHeight(Integer sampleHeight) {
        this.sampleHeight = sampleHeight;
    }

    public Recordtb getRecordtb() {
        return recordtb;
    }

    public void setRecordtb(Recordtb recordtb) {
        this.recordtb = recordtb;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recordId != null ? recordId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Illuminance)) {
            return false;
        }
        Illuminance other = (Illuminance) object;
        if ((this.recordId == null && other.recordId != null) || (this.recordId != null && !this.recordId.equals(other.recordId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "businessLogic.Illuminance[ recordId=" + recordId + " ]";
    }
    
}
